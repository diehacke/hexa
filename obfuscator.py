out_dir = 'src-obfuscated'
input_dirs = ['src', 'src-core']
ignore_classes = {'Main', 'IAssets'}

import os
import shutil
import re

class NameGenerator:
    index = 0
    def next_class_name(self):
        result = 'A' + str(self.index)
        self.index += 1
        return result
    def next_field_name(self):
        result = 'a' + str(self.index)
        self.index += 1
        return result

class Unit:
    name = None
    package = None
    text = None
    def full_name(self):
        if self.package == []:
            return self.name
        return '.'.join(self.package) + '.' + self.name
    def reldir(self):
        if self.package == []:
            return '.'
        return os.sep.join(self.package)
    out_name = None
    out_text = None
    out_move_up = False
    strings = None
    def hide_strings(self):
        self.strings = {}
        state = 0
        string_index = 0
        current_string = None
        out = []
        for c in self.out_text:
            if state == 0:
                if c == '"':
                    state = 1
                    current_string = '"'
                else:
                    out.append(c)
            elif state == 1:
                if c == '\\':
                    state = 2
                    current_string += c
                elif c == '"':
                    state = 0
                    current_string += '"'
                    key = '[@@@@' + str(string_index) + ']'
                    string_index += 1
                    out.append(key)
                    self.strings[key] = current_string
                else:
                    current_string += c
            elif state == 2:
                state = 1
                current_string += c
        self.out_text = ''.join(out)

    def show_strings(self):
        def replace(match):
            return self.strings[match.group()]
        r = re.compile(r'\[@@@@\d+\]')
        self.out_text = r.sub(replace, self.out_text)

def read_input():
    for input_dir in input_dirs:
        for root, _, files in os.walk(input_dir):
            reldir = os.path.relpath(root, input_dir)
            package = reldir.split(os.sep) if reldir != '.' and reldir != '' else []
            for file_ in files:
                file_name, ext = os.path.splitext(file_)
                if ext == '.hx':
                    unit = Unit()
                    unit.name = file_name
                    unit.package = package
                    file_path = os.path.join(root, file_)
                    with open(file_path, 'r') as f:
                        unit.text = f.read()
                    unit.out_name = unit.name
                    unit.out_text = unit.text
                    units.append(unit)

def replace():
    for unit in units:
        unit.hide_strings()

    for unit in units:
        if not unit.name in ignore_classes:
            unit.out_name = generator.next_class_name()
            if unit.package != []:
                unit.out_move_up = True
    for unit in units:
        if unit.out_move_up:
            unit.out_text = unit.out_text.replace('package ' + '.'.join(unit.package) + ';', 'package ;')
        for unit_j in units:
            if unit_j.out_move_up:
                import_text = 'import ' + unit_j.full_name()
                unit.out_text = unit.out_text.replace(import_text + ';\r\n', '')
                unit.out_text = unit.out_text.replace(import_text + ';\r', '')
                unit.out_text = unit.out_text.replace(import_text + ';\n', '')
                unit.out_text = unit.out_text.replace(unit_j.full_name(), unit_j.name)
        for unit_j in units:
            r = re.compile('\\b' + unit_j.name + '\\b')
            unit.out_text = r.sub(unit_j.out_name, unit.out_text)

    fields = set()
    field_r = re.compile('\\b_[A-Za-z0-9_]+\\b')
    for unit in units:
        unit_fields = field_r.findall(unit.out_text)
        for field in unit_fields:
            fields.add(field)
    out_fields = {}
    for field in fields:
        out_fields[field] = generator.next_field_name()
    def replace_field(match):
        return out_fields[match.group()]
    for unit in units:
        unit.out_text = field_r.sub(replace_field, unit.out_text)

    names = set()
    marked_name_r = re.compile(r'/\*\*/[A-Za-z0-9_]+\b')
    for unit in units:
        unit_names = marked_name_r.findall(unit.out_text)
        for name in unit_names:
            names.add(name[4:])
    print 'Custom replaced names: '
    print ', '.join(names)
    out_names = {}
    for name in names:
        out_names[name] = generator.next_class_name()
    def replace_name(match):
        name = match.group()
        return out_names[name] if name in out_names else name
    name_r = re.compile('\\b[A-Za-z0-9_]+\\b')
    for unit in units:
        unit.out_text = name_r.sub(replace_name, unit.out_text)

    for unit in units:
        unit.show_strings()

def write_output():
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for root, dirs, files in os.walk(out_dir):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
    for unit in units:
        full_dir = out_dir if unit.out_move_up else os.path.join(out_dir, unit.reldir())
        if not os.path.exists(full_dir):
            os.makedirs(full_dir)
        file_path = os.path.join(full_dir, unit.out_name + '.hx')
        with open(file_path, 'w') as f:
            f.write(unit.out_text)

generator = NameGenerator()
units = []
read_input()
replace()
write_output()

#unit = Unit()
#unit.name = 'Test'
#unit.out_text = r'fontTagText = addToRight(fontTagText, " face=\"" + format.font + "\"");'
#print unit.out_text
#unit.hide_strings()
#print unit.out_text
#unit.show_strings()
#print unit.out_text
