﻿package ;

class XY {
	public var x:Float = 0;
	public var y:Float = 0;
	
	public function new() {
	}

	public inline function set(v:XY):XY {
		x = v.x;
		y = v.y;
		return this;
	}

	public inline function setXY(x:Float, y:Float):XY {
		this.x = x;
		this.y = y;
		return this;
	}

	public inline function plus(v:XY):XY {
		x += v.x;
		y += v.y;
		return this;
	}

	public inline function minus(v:XY):XY {
		x -= v.x;
		y -= v.y;
		return this;
	}

	public inline function mult(s:Float):XY {
		x *= s;
		y *= s;
		return this;
	}

	public inline function normalize():XY {
		var k = 1 / Math.sqrt(x * x + y * y);
		x *= k;
		y *= k;
		return this;
	}

	public inline function fastNormalize():XY {
		var k = MathUtil.invSqrt(x * x + y * y);
		x *= k;
		y *= k;
		return this;
	}

	public inline function length():Float {
		return Math.sqrt(x * x + y * y);
	}

	public inline function fastLength():Float {
		return MathUtil.sqrt(x * x + y * y);
	}

	public inline function length2():Float {
		return x * x + y * y;
	}
	
	public inline function distance(v:XY):Float {
		var dx = v.x - x;
		var dy = v.y - y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	public inline function fastDistance(v:XY):Float {
		var dx = v.x - x;
		var dy = v.y - y;
		return MathUtil.sqrt(dx * dx + dy * dy);
	}

	public function distance2(v:XY):Float {
		var dx = v.x - x;
		var dy = v.y - y;
		return dx * dx + dy * dy;
	}

	public function toString():String {
		return "(" + x + ", " + y + ")";
	}
}
