package ;

import flash.display.Graphics;
import flash.display.BitmapData;
import flash.geom.Matrix;

class UIFrame {
	private static inline var TOLERANCE:Float = 2;

	private var _bd:BitmapData;
	inline public function bd():BitmapData {
		return _bd;
	}

	private var _x:Float;
	inline public function x():Float {
		return _x;
	}

	private var _y:Float;
	inline public function y():Float {
		return _y;
	}

	private var _w:Float;
	inline public function w():Float {
		return _w;
	}

	private var _h:Float;
	inline public function h():Float {
		return _h;
	}

	private var _offsetX:Float;
	inline public function offsetX():Float {
		return _offsetX;
	}

	private var _offsetY:Float;
	inline public function offsetY():Float {
		return _offsetY;
	}

	public function new(x:Float, y:Float, w:Float, h:Float, offsetX:Float, offsetY:Float) {
		_x = x;
		_y = y;
		_w = w;
		_h = h;
		_offsetX = offsetX;
		_offsetY = offsetY;
	}

	public function init(bd:BitmapData):Void {
		_bd = bd;
	}

	private static var _matrix:Matrix = new Matrix();
	private static var _scaleMatrix:Matrix = new Matrix();

	public function beginFill(g:Graphics, x:Float, y:Float):Void {
		_matrix.tx = -_x + x;
		_matrix.ty = -_y + y;
		g.beginBitmapFill(_bd, _matrix, false);
	}

	public function draw(g:Graphics, x:Float, y:Float):Void {
		_matrix.tx = -_x + x;
		_matrix.ty = -_y + y;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x + _offsetX - TOLERANCE, y + _offsetY - TOLERANCE, _w + TOLERANCE * 2, _h + TOLERANCE * 2);
		g.endFill();
	}

	public function drawTrimmed(g:Graphics, x:Float, y:Float):Void {
		_matrix.tx = -_x + x;
		_matrix.ty = -_y + y;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x + _offsetX, y + _offsetY, _w, _h);
		g.endFill();
	}

	public function drawSlicedV(
		g:Graphics, x:Float, y:Float, topH:Float, bottomH:Float, h:Float):Void {
		var x0 = _x + _offsetX;
		var y0 = _y + _offsetY;
		_matrix.tx = x - x0;
		_matrix.ty = y - y0;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x, y, _w, topH);
		g.endFill();
		if (h > topH + bottomH) {
			var k = (h - topH - bottomH) / (_h - topH - bottomH);
			_scaleMatrix.a = 1;
			_scaleMatrix.d = k;
			_scaleMatrix.tx = x - x0;
			_scaleMatrix.ty = y - y0 * k + topH * (1 - k);
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x, y + topH, _w, h - topH - bottomH);
			g.endFill();
		}
		_matrix.tx = x - x0;
		_matrix.ty = y - y0 - _h + h;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x, y + h - bottomH, _w, bottomH);
		g.endFill();
	}

	public function drawSliced(
		g:Graphics, x:Float, y:Float, w:Float, h:Float,
		leftW:Float, rightW:Float, topH:Float, bottomH:Float):Void {

		var x0 = _x + _offsetX;
		var y0 = _y + _offsetY;

		_matrix.tx = x - x0;
		_matrix.ty = y - y0;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x, y, leftW, topH);
		g.endFill();
		if (h > topH + bottomH) {
			var k = (h - topH - bottomH) / (_h - topH - bottomH);
			_scaleMatrix.a = 1;
			_scaleMatrix.d = k;
			_scaleMatrix.tx = x - x0;
			_scaleMatrix.ty = y - y0 * k + topH * (1 - k);
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x, y + topH, leftW, h - topH - bottomH);
			g.endFill();
		}
		_matrix.tx = x - x0;
		_matrix.ty = y - y0 - _h + h;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x, y + h - bottomH, leftW, bottomH);
		g.endFill();

		_matrix.tx = x - x0 - _w + w;
		_matrix.ty = y - y0;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x + w - rightW, y, rightW, topH);
		g.endFill();
		if (h > topH + bottomH) {
			var k = (h - topH - bottomH) / (_h - topH - bottomH);
			_scaleMatrix.a = 1;
			_scaleMatrix.d = k;
			_scaleMatrix.tx = x - x0 - _w + w;
			_scaleMatrix.ty = y - y0 * k + topH * (1 - k);
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x + w - rightW, y + topH, rightW, h - topH - bottomH);
			g.endFill();
		}
		_matrix.tx = x - x0 - _w + w;
		_matrix.ty = y - y0 - _h + h;
		g.beginBitmapFill(_bd, _matrix, false);
		g.drawRect(x + w - rightW, y + h - bottomH, rightW, bottomH);
		g.endFill();

		if (w > leftW + rightW) {
			var k = (w - leftW - rightW) / (_w - leftW - rightW);

			_scaleMatrix.a = k;
			_scaleMatrix.d = 1;
			_scaleMatrix.tx = x - x0 * k + leftW * (1 - k);
			_scaleMatrix.ty = y - y0;
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x + leftW, y, w - leftW - rightW, topH);
			g.endFill();

			_scaleMatrix.a = k;
			_scaleMatrix.d = 1;
			_scaleMatrix.tx = x - x0 * k + leftW * (1 - k);
			_scaleMatrix.ty = y - y0 - _h + h;
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x + leftW, y + h - bottomH, w - leftW - rightW, bottomH);
			g.endFill();
		}

		if (h > topH + bottomH && w > leftW + rightW) {
			var kx = (w - leftW - rightW) / (_w - leftW - rightW);
			var ky = (h - topH - bottomH) / (_h - topH - bottomH);

			_scaleMatrix.a = kx;
			_scaleMatrix.d = ky;
			_scaleMatrix.tx = x - x0 * kx + leftW * (1 - kx);
			_scaleMatrix.ty = y - y0 * ky + topH * (1 - ky);
			g.beginBitmapFill(_bd, _scaleMatrix, false);
			g.drawRect(x + leftW, y + topH, w - leftW - rightW, h - topH - bottomH);
			g.endFill();
		}
	}
}
