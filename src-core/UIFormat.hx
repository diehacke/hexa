package ;

import flash.filters.BitmapFilter;
import flash.geom.ColorTransform;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class UIFormat extends TextFormat
{
	public function new(?font:String, ?size:Float, ?color:Int) {
		super();
		this.font = font;
		this.size = size;
		this.color = color;
		alpha = Math.NaN;
	}
	
	public var embedFonts:Bool;
	public var filters:Array<BitmapFilter>;
	
	public function setFont(font:String, embedFonts:Bool = false):UIFormat {
		this.font = font;
		this.embedFonts = embedFonts;
		return this;
	}
	
	public function setSize(size:Int):UIFormat {
		this.size = size;
		return this;
	}
	
	public function setColor(color:Int):UIFormat {
		this.color = color;
		return this;
	}
	
	public function setBold(bold:Bool):UIFormat {
		this.bold = bold;
		return this;
	}
	
	public function setItalic(italic:Bool):UIFormat {
		this.italic = italic;
		return this;
	}
	
	public function setUnderline(underline:Bool):UIFormat {
		this.underline = underline;
		return this;
	}

	public function setCenter():UIFormat {
		align = TextFormatAlign.CENTER;
		return this;
	}

	public function setRight():UIFormat {
		align = TextFormatAlign.RIGHT;
		return this;
	}

	public function clearFilters():UIFormat {
		filters = null;
		return this;
	}

	public function addFilter(filter:BitmapFilter):UIFormat {
		if (filters == null)
			filters = new Array<BitmapFilter>();
		filters.push(filter);
		return this;
	}
	
	public var alpha:Float;
	
	public function setAlpha(alpha:Float):UIFormat {
		this.alpha = alpha;
		return this;
	}
	
	public var colorTransform:ColorTransform;
	
	public function setColorTransform(colorTransform:ColorTransform):UIFormat {
		this.colorTransform = colorTransform;
		return this;
	}
	
	public function clone():UIFormat {
		var format = new UIFormat(font, size, color);

		format.bold = bold;
		format.italic = italic;
		format.underline = underline;
		format.url = url;
		format.target = target;
		format.align = align;
		format.leftMargin = leftMargin;
		format.rightMargin = rightMargin;
		format.indent = indent;
		format.leading = leading;

		format.blockIndent = blockIndent;
		format.bullet = bullet;
		format.kerning = kerning;
		format.letterSpacing = letterSpacing;
		format.tabStops = tabStops;
		
		format.embedFonts = embedFonts;
		format.filters = filters;
		format.alpha = alpha;
		format.colorTransform = colorTransform;
		
		return format;
	}
	
	public function applyTo(textField:TextField, shakePlainTextForNme:Bool = false):Void {
		textField.setTextFormat(this);
		textField.defaultTextFormat = this;
		textField.embedFonts = embedFonts;
		textField.filters = filters;
		if (colorTransform != null) {
			textField.transform.colorTransform = colorTransform;
			if (!Math.isNaN(alpha)) {
				textField.alpha = alpha;
			}
		} else {
			textField.alpha = Math.isNaN(alpha) ? 1 : alpha;
		}
	}
	
	public function newFixed(selectable:Bool = false, text:String = null):TextField {
		var textField:TextField = new TextField();
		textField.selectable = selectable;
		applyTo(textField);
		if (text != null) {
			textField.text = text;
		}
		return textField;
	}
	
	public function newAutoSized(selectable:Bool = false, text:String = null):TextField {
		var textField:TextField = new TextField();
		textField.autoSize = TextFieldAutoSize.LEFT;
		textField.selectable = selectable;
		applyTo(textField);
		if (text != null) {
			textField.text = text;
		}
		return textField;
	}
	
	public function toHtml(text:String):String {
		return getHtml(this, text);
	}
	
	static var _nullFormat:UIFormat;
	
	public static function setNullFormat(tf:TextField):Void {
		if (_nullFormat == null) {
			_nullFormat = new UIFormat();
			
			_nullFormat.align = TextFormatAlign.LEFT;
			_nullFormat.blockIndent = 0;
			_nullFormat.bold = false;
			_nullFormat.bullet = false;
			_nullFormat.color = 0x000000;
			_nullFormat.font = "Times New Roman";
			_nullFormat.indent = 0;
			_nullFormat.italic = false;
			_nullFormat.kerning = false;
			_nullFormat.leading = 0;
			_nullFormat.leftMargin = 0;
			_nullFormat.letterSpacing = 0;
			_nullFormat.rightMargin = 0;
			_nullFormat.size = 12;
			_nullFormat.tabStops = [];
			_nullFormat.target = "";
			_nullFormat.underline = false;
			_nullFormat.url = "";
			_nullFormat.colorTransform = new ColorTransform();
		}
		_nullFormat.applyTo(tf);
	}
	
	public static function getHtml(format:TextFormat, text:String):String {
		var fontTagText = null;
		
		if (format.font != null) {
			fontTagText = addToRight(fontTagText, " face=\"" + format.font + "\"");
		}
		if (format.color != null) {
			fontTagText = addToRight(
				fontTagText, " color=\"#" + toHex(format.color) + "\"");
		}
		if (format.size != null) {
			fontTagText = addToRight(fontTagText, " size=\"" + format.size + "\"");
		}
		if (format.letterSpacing != null) {
			fontTagText = addToRight(
				fontTagText, " letterspacing=\"" + format.letterSpacing + "\"");
		}
		if (format.kerning) {
			fontTagText = addToRight(fontTagText, " kerning=\"1\"");
		}
		
		var beginTags = null;
		var endTags = null;
		if (fontTagText != null) {
			beginTags = "<font" + fontTagText + ">";
			endTags = "</font>";
		}
		
		if (format.bold) {
			beginTags = addToRight(beginTags, "<b>");
			endTags = addToLeft(endTags, "</b>");
		}
		if (format.italic) {
			beginTags = addToRight(beginTags, "<i>");
			endTags = addToLeft(endTags, "</i>");
		}
		if (format.underline) {
			beginTags = addToRight(beginTags, "<u>");
			endTags = addToLeft(endTags, "</u>");
		}
		
		return beginTags == null ? text : beginTags + text + endTags;
	}
	
	static inline function addToRight(nullableText:String, additon:String):String {
		return nullableText != null ? nullableText + additon : additon;
	}
	
	static inline function addToLeft(nullableText:String, additon:String):String {
		return nullableText != null ? additon + nullableText : additon;
	}

	public static function toHex(x:Int):String {
		var text = "";
		var begin = false;
		for (i in 0 ... 8) {
			var digit = (x & 0xf0000000) >>> 28;
			if (digit != 0) {
				begin = true;
			}
			if (begin) {
				switch (digit) {
					case 0: text += "0";
					case 1: text += "1";
					case 2: text += "2";
					case 3: text += "3";
					case 4: text += "4";
					case 5: text += "5";
					case 6: text += "6";
					case 7: text += "7";
					case 8: text += "8";
					case 9: text += "9";
					case 10: text += "a";
					case 11: text += "b";
					case 12: text += "c";
					case 13: text += "d";
					case 14: text += "e";
					case 15: text += "f";
				};
			}
			x <<= 4;
		}
		return text != "" ? text : "0";
	}
}
