package ;

class Selector<T> {
	public function new() {
		reset();
	}

	private var _first:Bool;
	private var _maxCriterion:Float;

	inline public function reset():Void {
		_first = true;
		_maxCriterion = 0.;
		_selected = null;
	}

	inline public function add(value:T, criterion:Float):Void {
		if (_first || _maxCriterion < criterion) {
			_first = false;
			_maxCriterion = criterion;
			_selected = value;
		}
	}

	private var _selected:T;
	inline private function getSelected():T {
		return _selected;
	}
}
