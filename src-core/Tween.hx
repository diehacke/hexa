package ;

import flash.Lib;
import haxe.ds.ObjectMap;

class Tween<T> {
	public static function easeIn(t:Float, b:Float, c:Float, d:Float):Float {
		var tratio = t / d;
		return c * tratio * tratio * tratio + b;
	}

	public static function easeOut(t:Float, b:Float, c:Float, d:Float):Float {
		return easeIn(d - t, b + c, -c, d);
	}

	public static function easeInOut(t:Float, b:Float, c:Float, d:Float):Float {
		var halfD = .5 * d;
		return t < halfD ?
			easeIn(2 * t, b, .5 * c, d) :
			easeIn(2 * (d - t), b + c, - .5 * c, d);
	}

	public static function easeOutBounce(t:Float, b:Float, c:Float, d:Float):Float {
		if ((t/=d) < (1/2.75))
			return c * (7.5625 * t * t) + b;
		if (t < (2/2.75))
			return c * (7.5625 * (t-=(1.5/2.75)) * t + .75) + b;
		if (t < (2.5/2.75))
			return c * (7.5625 * (t-=(2.25/2.75)) * t + .9375) + b;
		return c * (7.5625 * (t-=(2.625/2.75)) * t + .984375) + b;
	}

	public static function linear(t:Float, b:Float, c:Float, d:Float):Float {
		var tratio = t / d;
		return b + c * tratio;
	}

	public static function lerp(t:Float, value0:Float, value1:Float):Float {
		return value0 + (value1 - value0) * t;
	}

	private static var _tweenSet:ObjectMap<Dynamic, Tween<Dynamic>>;
	
	private static function register(key:Dynamic, tween:Tween<Dynamic>):Void {
		if (_tweenSet == null) {
			_tweenSet = new ObjectMap<Dynamic, Tween<Dynamic>>();
		}
		var oldTween = _tweenSet.get(key);
		if (oldTween != null) {
			oldTween.kill();
		}
		_tweenSet.set(key, tween);
	}
	
	private static function unregister(key:Dynamic, tween:Tween<Dynamic>):Void {
		if (_tweenSet != null) {
			var oldTween = _tweenSet.get(key);
			if (oldTween == tween) {
				_tweenSet.remove(key);
			}
		}
	}

	public static function doOnEnterFrame():Void {
		if (_tweenSet != null) {
			for (tween in _tweenSet) {
				tween.processEnterFrame();
			}
		}
	}
	
	public static function killByKey(key:Dynamic):Void {
		if (_tweenSet != null) {
			var tween = _tweenSet.get(key);
			if (tween != null) {
				tween.kill();
			}
		}
	}
	
	/**
	 * Create and start tween
	 * @param	target - object, that fields changing by tween
	 * @param	vars - has of end values of target fields
	 * @param	duration - tween time milliseconds
	 * @return	tween object (for additional parametrization or stop,
	 * parametrization is _not_ damage tween if it in this frame)
	 */
	public static function to<T>(target:T, duration:Int, vars:Dynamic, key:TweenKey = null, byFrames:Bool = false):Tween<T> {
		return new Tween(target, duration, vars, key != null ? key : target, byFrames);
	}

	public static function apply<T>(target:T, vars:Dynamic, key:TweenKey = null):Void {
		killByKey(key != null ? key : target);
		for (field in Reflect.fields(vars)) {
			Reflect.setProperty(target, field, Reflect.getProperty(vars, field));
		}
	}
	
	private var _vars:Dynamic;
	private var _startVars:Dynamic;
	private var _duration:Int;
	private var _byFrames:Bool;

	private var _startTime:Int;

	private function new(target:T, duration:Int, vars:Dynamic<Float>, key:Dynamic, byFrames:Bool) {
		framesSpeed = 1;

		_target = target;
		_vars = vars;
		_key = key;
		_byFrames = byFrames;
		_startVars = { };
		for (field in Reflect.fields(_vars)) {
			Reflect.setProperty(_startVars, field, Reflect.getProperty(target, field));
		}
		_duration = duration;
		_ease = easeIn;
		register(key, this);
		
		_startTime = _byFrames ? 0 : Lib.getTimer();
		_elapsedTime = 0;
	}
	
	private var _target:T;
	inline public function target():T {
		return _target;
	}

	private var _key:Dynamic;
	inline public function key():Dynamic {
		return _key;
	}
	
	private var _ease:Float->Float->Float->Float->Float;
	
	/**
	 * @param	ease function(t, b, c, d)
	 * t - elapsed time since tween start, current time
	 * b - start value, value0
	 * c - change, value1 - value0, delta value
	 * d - duration, t1 - t0, delta time
	 * Easing function mast returns current value
	 * 
	 * b + c |                  **
	 *     / |             *****
	 *  c {  |       ******------- returned value
	 *     \ | ******     |
	 *     b -------------|-------
	 *       |            t      |
	 *       |<------ d -------->|
	 * 
	 * @return this
	 */
	public function setEase(ease:Float->Float->Float->Float->Float):Tween<T> {
		_ease = ease;
		return this;
	}
	
	private var _onComplete:Tween<T>->Void;
	
	public function setOnComplete(onComplete:Tween<T>->Void):Tween<T> {
		_onComplete = onComplete;
		return this;
	}
	
	private var _voidOnComplete:Void->Void;
	
	public function setVoidOnComplete(voidOnComplete:Void->Void):Tween<T> {
		_voidOnComplete = voidOnComplete;
		return this;
	}
	
	private var _onUpdate:Tween<T>->Void;
	
	public function setOnUpdate(onUpdate:Tween<T>->Void):Tween<T> {
		_onUpdate = onUpdate;
		return this;
	}
	
	private var _voidOnUpdate:Void->Void;
	
	public function setVoidOnUpdate(voidOnUpdate:Void->Void):Tween<T> {
		_voidOnUpdate = voidOnUpdate;
		return this;
	}

	private var _wait:Int;

	public function setWait(duration:Int):Tween<T> {
		_wait = duration;
		return this;
	}
	
	private var _elapsedTime:Int;

	public var framesSpeed:Int;
	
	private function processEnterFrame():Void {
		if (_wait > 0) {
			if (_elapsedTime >= _wait) {
				_wait = 0;
				if (_byFrames) {
					_startTime = 0;
					_elapsedTime = 0;
				} else {
					_startTime = Lib.getTimer();
				}
			}
		}
		if (_byFrames)
			_elapsedTime += framesSpeed;
		else
			_elapsedTime = Lib.getTimer() - _startTime;
		if (_wait > 0)
			return;
		if (_elapsedTime >= _duration) {
			for (field in Reflect.fields(_vars)) {
				var endValue:Float = Reflect.getProperty(_vars, field);
				Reflect.setProperty(_target, field, endValue);
			}
			if (_onUpdate != null) {
				_onUpdate(this);
			}
			if (_voidOnUpdate != null) {
				_voidOnUpdate();
			}
			kill(true);
		} else {
			for (field in Reflect.fields(_vars)) {
				var startValue:Float = Reflect.getProperty(_startVars, field);
				var endValue:Float = Reflect.getProperty(_vars, field);
				var value = _ease(_elapsedTime, startValue, endValue - startValue, _duration);
				Reflect.setProperty(_target, field, value);
			}
			if (_onUpdate != null) {
				_onUpdate(this);
			}
			if (_voidOnUpdate != null) {
				_voidOnUpdate();
			}
		}
	}
	
	public function kill(callComplete:Bool = false):Void {
		unregister(_key, this);
		if (callComplete) {
			if (_onComplete != null) {
				_onComplete(this);
			}
			if (_voidOnComplete != null) {
				_voidOnComplete();
			}
		}
	}
	
	public function value(startValue:Float, endValue:Float):Float {
		if (_wait > 0)
			return startValue;
		if (_elapsedTime >= _duration) {
			return endValue;
		}
		return _ease(_elapsedTime, startValue, endValue - startValue, _duration);
	}
	
	public function setVars(vars:Dynamic):Void {
		_vars = vars;
	}
}
