package ;

class RotationMatrix {
	public function new() {
	}

	public var a:Float = 1;
	public var b:Float = 0;
	public var c:Float = 0;
	public var d:Float = 1;

	inline public function rotate(angle:Float):RotationMatrix {
		var cos = Math.cos(angle);
		var sin = Math.sin(angle);
		a = cos;
		b = -sin;
		c = sin;
		d = cos;
		return this;
	}

	inline public function rotateYTo(directionX:Float, directionY:Float):RotationMatrix {
		a = -directionY;
		b = -directionX;
		c = directionX;
		d = -directionY;
		return this;
	}

	inline public function rotateXTo(directionX:Float, directionY:Float):RotationMatrix {
		a = directionX;
		b = -directionY;
		c = directionY;
		d = directionX;
		return this;
	}

	public var outX:Float;
	public var outY:Float;

	inline public function transform(x:Float, y:Float):Void {
		outX = x * a + y * b;
		outY = x * c + y * d;
	}
}
