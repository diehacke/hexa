package ;

class MathUtil {
	public static function __init__() {
#if flash
		var b = new flash.utils.ByteArray();
		b.length = 1024;
		flash.Memory.select(b);
#end
	}

	inline public static function invSqrt(x:Float):Float {
#if flash
	var half = 0.5 * x;
	flash.Memory.setFloat(0,x);
	var i = flash.Memory.getI32(0);
	i = 0x5f3759df - (i>>1);
	flash.Memory.setI32(0,i);
	x = flash.Memory.getFloat(0);
	x = x * (1.5 - half*x*x);
	return x;
#else
	return 1 / Math.sqrt(x);
#end
	}

	inline public static function sign(x:Int):Int {
		var result = 0;
		if (x > 0)
			result = 1;
		else if (x < 0)
			result = -1;
		return result;
	}

	inline public static function abs(x:Float):Float {
		return x >= 0 ? x : -x;
	}

	inline public static function lerp(x0:Float, x1:Float, t:Float):Float {
		return x0 + (x1 - x0) * t;
	}

	inline public static function clamp(value:Float, min:Float, max:Float):Float {
		return if (value < min)
			min;
		else if (value > max)
			max;
		else
			value;
	}

	inline public static function sqrt(x:Float):Float {
#if flash
		return 1 / invSqrt(x);
#else
		return Math.sqrt(x);
#end
	}

	inline public static function getRotation(dx:Float, dy:Float):Float {
		return ((atan2(dy, dx) + Math.PI / 2) / Math.PI) * 180;
	}

	inline public static function getAngle(x0:Float, y0:Float, x1:Float, y1:Float):Float {
		return atan2(x0 * y1 - y0 * x1, x0 * x1 + y0 * y1);
	}

	inline public static function distance2(x0:Float, y0:Float, x1:Float, y1:Float):Float {
		return (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0);
	}

	inline public static function distance(x0:Float, y0:Float, x1:Float, y1:Float):Float {
		return sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
	}

	private static var ONEQTR_PI:Float = Math.PI / 4.0;
	private static var THRQTR_PI:Float = 3.0 * Math.PI / 4.0;

	inline public static function atan2(y:Float, x:Float):Float {
		var r:Float;
		var angle:Float;
		var abs_y:Float = abs(y) + 1e-10;      // kludge to prevent 0/0 condition
		if (x < .0) {
			r = (x + abs_y) / (abs_y - x);
			angle = THRQTR_PI;
		} else {
			r = (x - abs_y) / (x + abs_y);
			angle = ONEQTR_PI;
		}
		angle += (0.1963 * r * r - 0.9817) * r;
		return y < .0 ? -angle : angle;     // negate if in quad III or IV
	}

	public static var outX:Float;
	public static var outY:Float;

	public static function segmentsCrossing(
		x1:Float, y1:Float, x2:Float, y2:Float,
		x1_:Float, y1_:Float, x2_:Float, y2_:Float):Bool {
		var a = y1 - y2;
		var b = x2 - x1;
		var c = x1 * y2 - x2 * y1;
		var a_ = y1_ - y2_;
		var b_ = x2_ - x1_;
		var c_ = x1_ * y2_ - x2_ * y1_;
		return if (
			((b_ * (y1 - y1_) + a_ * (x1 - x1_)) * (b_ * (y2 - y1_) + a_ * (x2 - x1_)) < .0) &&
			((b * (y1_ - y1) + a * (x1_ - x1)) * (b * (y2_ - y1) + a * (x2_ - x1)) < .0)
		) {
			var k = 1 / (a * b_ - a_ * b);
			outX = (b * c_ - b_ * c) * k;
			outY = (c * a_ - c_ * a) * k;
			true;
		} else {
			outX = .0;
			outY = .0;
			false;
		}
	}
}
