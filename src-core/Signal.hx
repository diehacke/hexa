package ;

class Signal<TArg>
{
	public function new() {
		_listeners = [];
		_length = 0;
		_voidListeners = [];
		_voidLength = 0;
	}
	
	private var _listeners:Array<TArg->Void>;
	private var _length:Int;
	
	public function add(listener:TArg->Void):Void {
		for (i in 0 ... _length) {
			if (_listeners[i] == listener) {
				return;
			}
		}
		_listeners[_length++] = listener;
	}
	
	public function remove(listener:TArg->Void):Void {
		for (i in 0 ... _length) {
			if (_listeners[i] == listener) {
				_listeners[i] = _listeners[_length - 1];
				_listeners[_length - 1] = null;
				_length--;
				break;
			}
		}
	}
	
	private var _voidListeners:Array<Void->Void>;
	private var _voidLength:Int;
	
	public function addVoid(listener:Void->Void):Void {
		for (i in 0 ... _voidLength) {
			if (_voidListeners[i] == listener) {
				return;
			}
		}
		_voidListeners[_voidLength++] = listener;
	}
	
	public function removeVoid(listener:Void->Dynamic):Void {
		for (i in 0 ... _voidLength) {
			if (_voidListeners[i] == listener) {
				_voidListeners[i] = _voidListeners[_voidLength - 1];
				_voidListeners[_voidLength - 1] = null;
				_voidLength--;
				break;
			}
		}
	}
	
	public function dispatch(arg:TArg):Void {
		var voidListeners:Array<Void->Void> = null;
		if (_voidLength > 0) {
			voidListeners = [];
			for (i in 0 ... _voidLength) {
				voidListeners[i] = _voidListeners[i];
			}
		}
		var listeners:Array<TArg->Void> = null;
		if (_length > 0) {
			listeners = [];
			for (i in 0 ... _length) {
				listeners[i] = _listeners[i];
			}
		}
		if (listeners != null) {
			for (listener in listeners) {
				listener(arg);
			}
		}
		if (voidListeners != null) {
			for (listener in voidListeners) {
				listener();
			}
		}
	}
}
