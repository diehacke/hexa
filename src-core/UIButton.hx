package ;

import flash.events.MouseEvent;
import flash.display.Stage;

class UIButton extends UITile {
	public function new() {
		super();
		
		mouseEnabled = true;
		mouseChildren = false;
		
		_enabled = true;
		_selected = false;
		_isDown = false;
		_isOver = false;
		
		updateEnabled();
	}

	private var _isDown:Bool;
	inline public function isDown():Bool {
		return _isDown;
	}

	private var _isOver:Bool;
	inline public function isOver():Bool {
		return _isOver;
	}
	
	private var _enabled:Bool;
	inline public function enabled():Bool {
		return _enabled;
	}
	public function setEnabled(value:Bool):Void {
		if (_enabled != value) {
			_enabled = value;
			updateEnabled();
			redraw();
		}
	}
	
	private function updateEnabled() {
		buttonMode = _enabled;
		if (_enabled) {
			removeEventListener(MouseEvent.CLICK, onBlockMouse);
			removeEventListener(MouseEvent.MOUSE_DOWN, onBlockMouse);
			removeEventListener(MouseEvent.MOUSE_UP, onBlockMouse);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		} else {
			_isOver = false;
			_isDown = false;
			
			addEventListener(MouseEvent.CLICK, onBlockMouse, false, 100500);
			addEventListener(MouseEvent.MOUSE_DOWN, onBlockMouse, false, 100500);
			addEventListener(MouseEvent.MOUSE_UP, onBlockMouse, false, 100500);
			
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			if (_stage != null) {
				_stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
			}
		}
	}
	
	private function onBlockMouse(event:MouseEvent):Void {
		event.stopImmediatePropagation();
	}

	private var _stage:Stage;
	
	private function onMouseDown(event:MouseEvent):Void {
		if (!_enabled) {
			event.stopImmediatePropagation();
			return;
		}
		
		if (stage == null) {
			return;
		}
		_stage = stage;
		_stage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
		_isDown = true;
		redraw();
	}
	
	private function onStageMouseUp(event:MouseEvent):Void {
		_stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
		if (!_enabled) {
			event.stopImmediatePropagation();
			return;
		}
		
		_isDown = false;
		redraw();
	}

	private function onRollOver(event:MouseEvent):Void {
		_isOver = true;
		redraw();
	}

	private function onRollOut(event:MouseEvent):Void {
		_isOver = false;
		redraw();
	}
	
	public function processClick():Void {
		if (_enabled)
			dispatchEvent(new MouseEvent(MouseEvent.CLICK));
	}
}
