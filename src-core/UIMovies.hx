// <<Generated>>
package ;

class UIMovies {
	inline public static var title_normal:Int = 0;

	public static var title:Array<UIFrame> = [
		UIFrames.title_normal
	];

	inline public static var records_normal:Int = 0;

	public static var records:Array<UIFrame> = [
		UIFrames.records_normal
	];

	inline public static var record_normal:Int = 0;

	public static var record:Array<UIFrame> = [
		UIFrames.record_normal
	];

	inline public static var stars_slot_1:Int = 0;
	inline public static var stars_slot_2:Int = 1;
	inline public static var stars_slot_3:Int = 2;
	inline public static var stars_star_1:Int = 3;
	inline public static var stars_star_2:Int = 4;
	inline public static var stars_star_3:Int = 5;

	public static var stars:Array<UIFrame> = [
		UIFrames.stars_slot_1,
		UIFrames.stars_slot_2,
		UIFrames.stars_slot_3,
		UIFrames.stars_star_1,
		UIFrames.stars_star_2,
		UIFrames.stars_star_3
	];

	inline public static var button_image_restart:Int = 0;
	inline public static var button_image_new:Int = 1;
	inline public static var button_image_menu:Int = 2;

	public static var button_image:Array<UIFrame> = [
		UIFrames.button_image_restart,
		UIFrames.button_image_new,
		UIFrames.button_image_menu
	];

	inline public static var button_up:Int = 0;
	inline public static var button_over:Int = 1;
	inline public static var button_down:Int = 2;
	inline public static var button_disabled:Int = 3;

	public static var button:Array<UIFrame> = [
		UIFrames.button_up,
		UIFrames.button_over,
		UIFrames.button_down,
		UIFrames.button_disabled
	];

	inline public static var thin_button_up:Int = 0;
	inline public static var thin_button_over:Int = 1;
	inline public static var thin_button_down:Int = 2;
	inline public static var thin_button_selected_up:Int = 3;
	inline public static var thin_button_selected_over:Int = 4;
	inline public static var thin_button_selected_down:Int = 5;

	public static var thin_button:Array<UIFrame> = [
		UIFrames.thin_button_up,
		UIFrames.thin_button_over,
		UIFrames.thin_button_down,
		UIFrames.thin_button_selected_up,
		UIFrames.thin_button_selected_over,
		UIFrames.thin_button_selected_down
	];

	inline public static var confirmation_normal:Int = 0;

	public static var confirmation:Array<UIFrame> = [
		UIFrames.confirmation_normal
	];

	inline public static var game_over_bg:Int = 0;

	public static var game_over:Array<UIFrame> = [
		UIFrames.game_over_bg
	];

	inline public static var bubble_blue_normal:Int = 0;
	inline public static var bubble_blue_down:Int = 1;
	inline public static var bubble_blue_explosion_1:Int = 2;
	inline public static var bubble_blue_explosion_2:Int = 3;
	inline public static var bubble_blue_explosion_3:Int = 4;
	inline public static var bubble_blue_rise_0:Int = 5;
	inline public static var bubble_blue_rise_1:Int = 6;
	inline public static var bubble_blue_rise_2:Int = 7;
	inline public static var bubble_blue_rise_3:Int = 8;

	public static var bubble_blue:Array<UIFrame> = [
		UIFrames.bubble_blue_normal,
		UIFrames.bubble_blue_down,
		UIFrames.bubble_blue_explosion_1,
		UIFrames.bubble_blue_explosion_2,
		UIFrames.bubble_blue_explosion_3,
		UIFrames.bubble_blue_rise_0,
		UIFrames.bubble_blue_rise_1,
		UIFrames.bubble_blue_rise_2,
		UIFrames.bubble_blue_rise_3
	];

	inline public static var bubble_green_normal:Int = 0;
	inline public static var bubble_green_down:Int = 1;
	inline public static var bubble_green_explosion_1:Int = 2;
	inline public static var bubble_green_explosion_2:Int = 3;
	inline public static var bubble_green_explosion_3:Int = 4;
	inline public static var bubble_green_rise_0:Int = 5;
	inline public static var bubble_green_rise_1:Int = 6;
	inline public static var bubble_green_rise_2:Int = 7;
	inline public static var bubble_green_rise_3:Int = 8;

	public static var bubble_green:Array<UIFrame> = [
		UIFrames.bubble_green_normal,
		UIFrames.bubble_green_down,
		UIFrames.bubble_green_explosion_1,
		UIFrames.bubble_green_explosion_2,
		UIFrames.bubble_green_explosion_3,
		UIFrames.bubble_green_rise_0,
		UIFrames.bubble_green_rise_1,
		UIFrames.bubble_green_rise_2,
		UIFrames.bubble_green_rise_3
	];

	inline public static var bubble_purple_normal:Int = 0;
	inline public static var bubble_purple_down:Int = 1;
	inline public static var bubble_purple_explosion_1:Int = 2;
	inline public static var bubble_purple_explosion_2:Int = 3;
	inline public static var bubble_purple_explosion_3:Int = 4;
	inline public static var bubble_purple_rise_0:Int = 5;
	inline public static var bubble_purple_rise_1:Int = 6;
	inline public static var bubble_purple_rise_2:Int = 7;
	inline public static var bubble_purple_rise_3:Int = 8;

	public static var bubble_purple:Array<UIFrame> = [
		UIFrames.bubble_purple_normal,
		UIFrames.bubble_purple_down,
		UIFrames.bubble_purple_explosion_1,
		UIFrames.bubble_purple_explosion_2,
		UIFrames.bubble_purple_explosion_3,
		UIFrames.bubble_purple_rise_0,
		UIFrames.bubble_purple_rise_1,
		UIFrames.bubble_purple_rise_2,
		UIFrames.bubble_purple_rise_3
	];

	inline public static var bubble_red_normal:Int = 0;
	inline public static var bubble_red_down:Int = 1;
	inline public static var bubble_red_explosion_1:Int = 2;
	inline public static var bubble_red_explosion_2:Int = 3;
	inline public static var bubble_red_explosion_3:Int = 4;
	inline public static var bubble_red_rise:Int = 5;
	inline public static var bubble_red_rise_1:Int = 6;
	inline public static var bubble_red_rise_2:Int = 7;
	inline public static var bubble_red_rise_3:Int = 8;

	public static var bubble_red:Array<UIFrame> = [
		UIFrames.bubble_red_normal,
		UIFrames.bubble_red_down,
		UIFrames.bubble_red_explosion_1,
		UIFrames.bubble_red_explosion_2,
		UIFrames.bubble_red_explosion_3,
		UIFrames.bubble_red_rise,
		UIFrames.bubble_red_rise_1,
		UIFrames.bubble_red_rise_2,
		UIFrames.bubble_red_rise_3
	];

	inline public static var bubble_yellow_normal:Int = 0;
	inline public static var bubble_yellow_down:Int = 1;
	inline public static var bubble_yellow_explosion_1:Int = 2;
	inline public static var bubble_yellow_explosion_2:Int = 3;
	inline public static var bubble_yellow_explosion_3:Int = 4;
	inline public static var bubble_yellow_rise_0:Int = 5;
	inline public static var bubble_yellow_rise_1:Int = 6;
	inline public static var bubble_yellow_rise_2:Int = 7;
	inline public static var bubble_yellow_rise_3:Int = 8;

	public static var bubble_yellow:Array<UIFrame> = [
		UIFrames.bubble_yellow_normal,
		UIFrames.bubble_yellow_down,
		UIFrames.bubble_yellow_explosion_1,
		UIFrames.bubble_yellow_explosion_2,
		UIFrames.bubble_yellow_explosion_3,
		UIFrames.bubble_yellow_rise_0,
		UIFrames.bubble_yellow_rise_1,
		UIFrames.bubble_yellow_rise_2,
		UIFrames.bubble_yellow_rise_3
	];

	inline public static var sound_button_1_normal:Int = 0;
	inline public static var sound_button_1_over:Int = 1;
	inline public static var sound_button_1_selected:Int = 2;
	inline public static var sound_button_1_selected_over:Int = 3;

	public static var sound_button_1:Array<UIFrame> = [
		UIFrames.sound_button_1_normal,
		UIFrames.sound_button_1_over,
		UIFrames.sound_button_1_selected,
		UIFrames.sound_button_1_selected_over
	];

	inline public static var sound_button_2_normal:Int = 0;
	inline public static var sound_button_2_over:Int = 1;
	inline public static var sound_button_2_selected:Int = 2;
	inline public static var sound_button_2_selected_over:Int = 3;

	public static var sound_button_2:Array<UIFrame> = [
		UIFrames.sound_button_2_normal,
		UIFrames.sound_button_2_over,
		UIFrames.sound_button_2_selected,
		UIFrames.sound_button_2_selected_over
	];
}
