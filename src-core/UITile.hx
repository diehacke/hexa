package ;

class UITile extends UIWidget {
	public function new() {
		super();
	}

	private var _selected:Bool;
	inline public function selected():Bool {
		return _selected;
	}
	public function setSelected(value:Bool):Void {
		if (_selected != value) {
			_selected = value;
			redraw();
		}
	}
}
