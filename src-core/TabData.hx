package ;

import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.MacroStringTools;

class TabData {
#if macro
	private static var _tabs:Int;
	private static var _lines:Array<{tabs:Int, text:String}>;
#end

	macro public static function execute(text:String):Expr {
		text = ~/\n\r/g.replace(text, "\n");
		text = ~/\r/g.replace(text, "\n");
		_lines = new Array<{tabs:Int, text:String}>();
		for (line in text.split("\n")) {
			var tabs = 0;
			var varIndex = 0;
			while(tabs < line.length && line.charAt(tabs) == "\t") {
				tabs++;
			}
			line = line.substr(tabs);
			if (line.length == 0)
				continue;
			if (line.charAt(0) == "'")
				continue;
			if (line.charAt(0) == "|") {
				var index = line.indexOf(" ");
				if (index != -1) {
					var first = true;
					for (subline in line.split(" ")) {
						if (subline.length > 0) {
							if (first) {
								first = false;
								_lines.push({tabs:tabs, text:subline});
							} else {
								_lines.push({tabs:tabs + 1, text:subline});
							}
						}
					}
					continue;
				}
			}
			_lines.push({tabs:tabs, text:line});
		}
		var result = macro {};
		if (_lines.length > 0) {
			var line = _lines.shift();
			_tabs = line.tabs;
			result = executeNode(line);
		}
		return result;
	}

#if macro
	private static function executeNode(line:{tabs:Int, text:String}):Expr {
		var tabs = line.tabs;
		if (tabs != _tabs) {
			Context.error("Tabs disbalance", Context.currentPos());
			_tabs--;
			return macro {};
		}
		if (line == null) {
			_tabs--;
			return macro {};
		}
		if (line.text.charAt(0) == "|") {
			Context.error("Unexpected '|'", Context.currentPos());
			_tabs--;
			return macro {};
		}
		var expr = macro ${Context.parse(line.text, Context.currentPos())};
		while (true) {
			var lineI = _lines.shift();
			if (lineI == null)
				break;
			if (lineI.tabs < tabs) {
				_lines.unshift(lineI);
				break;
			}
			if (lineI.text.charAt(0) == "|") {
				if (lineI.tabs < tabs) {
					break;
				}
				if (lineI.tabs > tabs) {
					Context.error(
						"Incorrect tabs for '" + lineI.text + "'(" + lineI.tabs + ") after '" + line.text +
						"'(" + tabs + ")",
						Context.currentPos());
					break;
				}
				var method = lineI.text.substr(1);
				var subline = _lines.shift();
				if (subline.tabs - tabs != 1) {
					Context.error("Incorrect tabs after '|'", Context.currentPos());
					break;
				}
				_tabs++;
				expr = macro ${expr}.$method(${executeNode(subline)});
			}
		}
		_tabs--;
		return expr;
	}
#end
}
