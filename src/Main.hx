package ;

import flash.net.SharedObject;
import flash.Lib;
import flash.display.Stage;
import flash.events.KeyboardEvent;
import flash.display.StageScaleMode;
import flash.display.StageAlign;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import haxe.Log;

class Main extends UIWidget {
	public static function main():Void {
		Lib.current.addChild(new Main());
	}

	public function new() {
		super();
		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
	}

	private var _model:Model;
	private var _player:Player;
	private var _soundPanel:SoundPanel;

	private function onAddedToStage(event:Event):Void {
		removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

		StartHelper.check(["www.flashgamelicense.com"], {
			Log.setColor(0x00ff00);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			UIFrames.init();
			UIKeys.init();

			_player = new Player();
			_player.connectTo(this);

			stage.addEventListener(Event.RESIZE, onStageResize);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);

			_soundPanel = new SoundPanel(_player);
			_soundPanel.redraw();
			addChild(_soundPanel);

			updateSize();
			_model = new Model();
			setScreen(new Menu());
		});
	}

	private var _screen:AScreen;

	public function setScreen(screen:AScreen):Void {
		if (_screen != null) {
			_screen.doOff();
			if (_screen.parent != null)
				_screen.parent.removeChild(_screen);
		}
		_screen = screen;
		if (_screen != null)
		{
			addChildAt(_screen, 0);
			_screen.w = w;
			_screen.h = h;
			_screen.init(this, _model, _player);
			_screen.doOn();
			_screen.redraw();
		}
	}

	private function updateSize():Void {
		w = stage.stageWidth;
		h = stage.stageHeight;

		var g = graphics;
		g.clear();
		g.beginBitmapFill(UIImages.bg);
		g.drawRect(0, 0, w, h);
		g.endFill();

		if (_soundPanel != null) {
			_soundPanel.x = w - _soundPanel.w;
			_soundPanel.y = h - _soundPanel.h;
		}
	}

	private function onStageResize(event:Event):Void {
		updateSize();
		w = stage.stageWidth;
		h = stage.stageHeight;
		if (_screen != null) {
			_screen.w = w;
			_screen.h = h;
			_screen.redraw();
		}
	}

	private function onKeyDown(event:KeyboardEvent):Void {
		if (_screen != null)
			_screen.doOnKeyDown(event.keyCode);
	}

	private function onMouseDown(event:MouseEvent):Void {
		if (_screen != null)
			_screen.doOnMouseDown();
	}

	private function onMouseUp(event:MouseEvent):Void {
		if (_screen != null)
			_screen.doOnMouseUp();
	}

	private var _debugMonitor:DebugMonitor;
	private var _switchDebugFlag:Bool;
	private var _switchDebugMonitor:Bool;

	private function onEnterFrame(event:Event):Void {
		Tween.doOnEnterFrame();
		_player.update();
		if (UIKeys.isDown(UIKeys.E) &&
			UIKeys.isDown(UIKeys.N6) &&
			UIKeys.isDown(UIKeys.U)) {
			if (!_switchDebugFlag) {
				_switchDebugFlag = true;
				if (_screen != null)
					_screen.doOnDebugPress();
			}
		} else {
			_switchDebugFlag = false;
		}
		if (UIKeys.isDown(UIKeys.Q) &&
			UIKeys.isDown(UIKeys.E) &&
			UIKeys.isDown(UIKeys.T)) {
			if (!_switchDebugMonitor) {
				_switchDebugMonitor = true;
				if (_debugMonitor == null) {
					_debugMonitor = new DebugMonitor();
					stage.addChild(_debugMonitor);
				} else {
					stage.removeChild(_debugMonitor);
					_debugMonitor = null;
				}
			}
		} else {
			_switchDebugMonitor = false;
		}
		if (_screen != null)
			_screen.doOnEnterFrame();
	}
}
