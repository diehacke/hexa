package ;

import flash.events.MouseEvent;

class SoundPanel extends UIWidget {
	private var _player:Player;
	private var _sound:SoundButton;
	private var _music:SoundButton;

	public function new(player:Player) {
		super();

		_player = player;

		_sound = new SoundButton(false);
		_sound.redraw();
		_sound.addEventListener(MouseEvent.MOUSE_DOWN, onSoundMouseDown);
		addChild(_sound);

		_music = new SoundButton(true);
		_music.redraw();
		_music.addEventListener(MouseEvent.MOUSE_DOWN, onMusicMouseDown);
		addChild(_music);

		updateState();
	}

	override public function redraw():Void {
		w = 72;
		h = 36;

		_music.x = 36;
	}

	private function onSoundMouseDown(event:MouseEvent):Void {
		_player.setSoundMuted(!_player.soundMuted());
		updateState();
	}

	private function onMusicMouseDown(event:MouseEvent):Void {
		_player.setMusicMuted(!_player.musicMuted());
		updateState();
	}

	private function updateState():Void {
		_sound.setSelected(!_player.soundMuted());
		_music.setSelected(!_player.musicMuted());
	}
}

