package ;

import flash.events.MouseEvent;
import flash.display.Shape;

class Menu extends AScreen {
	public function new() {
		super();
	}

	private var _title:Shape;
	private var _buttons:Array<MenuButton>;

	override public function doOn():Void {
		_model.fix();

		_title = new Shape();
		UIFrames.title_normal.draw(_title.graphics, 0, 0);
		addChild(_title);

		_buttons = new Array<MenuButton>();

		var button = new MenuButton();
		button.setText("PLAY");
		button.redraw();
		button.addEventListener(MouseEvent.CLICK, onStartClick);
		addChild(button);
		_buttons.push(button);

		var button = new MenuButton();
		button.setText("REPLAY LAST");
		button.redraw();
		button.setEnabled(_model.seed() != 0);
		button.addEventListener(MouseEvent.CLICK, onReplayClick);
		addChild(button);
		_buttons.push(button);

		var button = new MenuButton();
		button.setText("RECORDS");
		button.redraw();
		button.setEnabled(_model.length() > 0);
		button.addEventListener(MouseEvent.CLICK, onRecordsClick);
		addChild(button);
		_buttons.push(button);

		_player.setMusic(true);
	}

	override public function doOff():Void {
	}

	override public function redraw():Void {
		_title.x = Std.int(w * .5);
		_title.y = 100;

		var buttonsH = _buttons.length * 80;
		for (i in 0 ... _buttons.length) {
			var button = _buttons[i];
			button.x = Std.int(w * .5 - button.w * .5);
			button.y = Std.int(h * .5 - buttonsH * .5 + i * 80);
		}
	}

	private function onStartClick(event:MouseEvent):Void {
		_main.setScreen(new Game(true));
	}

	private function onReplayClick(event:MouseEvent):Void {
		_main.setScreen(new Game(false));
	}

	private function onRecordsClick(event:MouseEvent):Void {
		_main.setScreen(new Records());
	}
}
