package ;

class AScreen extends UIWidget {
	public function new() {
		super();
	}

	private var _main:Main;
	private var _model:Model;
	private var _player:Player;

	public function init(main:Main, model:Model, player:Player):Void {
		_main = main;
		_model = model;
		_player = player;
	}

	public function doOn():Void {
	}

	public function doOff():Void {
	}

	public function doOnKeyDown(keyCode:UInt):Void {
	}

	public function doOnMouseDown():Void {
	}

	public function doOnMouseUp():Void {
	}

	public function doOnDebugPress():Void {
	}

	public function doOnEnterFrame():Void {
	}
}
