package ;

import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.net.SharedObject;

typedef PlayerData = {
	soundMuted:Bool,
	musicMuted:Bool
}

class Player {
	private static var FILE_NAME:String = "hexa_sound_settings.dat";

	public function new() {
		_musicOn = false;
		load();
	}

	public function /**/playSound(sound:Sound, ?volume:Float) {
		if (!_soundMuted) {
			if (volume != null)
				sound.play(0, 1, new SoundTransform(volume));
			else
				sound.play();
		}
	}

	private var _solelySound:PlayerSound;

	public function /**/playSoundSolely(sound:Sound, volume:Float, ?loops:Int):Void {
		stopSoundSolely();
		_solelySound = new PlayerSound(sound, 0, loops, null);
		_solelySound.volume = volume;
		_solelySound.muted = _soundMuted;
	}

	public function /**/stopSoundSolely():Void {
		if (_solelySound != null) {
			_solelySound.channel.stop();
			_solelySound = null;
		}
	}

	private var _musicSound:PlayerSound;

	public function /**/stopMusic():Void {
		if (_musicSound != null) {
			_musicSound.channel.stop();
			_musicSound = null;
		}
	}

	public function /**/playMusic(sound:Sound, volume:Float, startTime:Float = 0):Void {
		stopMusic();
		_musicSound = new PlayerSound(sound, startTime, 1, onMusicComplete);
		_musicSound.volume = volume;
		_musicSound.muted = _musicMuted;
	}

	private function onMusicComplete(sound:PlayerSound):Void {
		sound.play(0, 1);
	}

	private var _soundMuted:Bool;
	public function soundMuted():Bool {
		return _soundMuted;
	}
	public function setSoundMuted(value:Bool):Bool {
		if (_soundMuted != value) {
			_soundMuted = value;
			if (_solelySound != null)
				_solelySound.muted = _soundMuted;
			save();
		}
		return _soundMuted;
	}

	private var _musicMuted:Bool;
	public function musicMuted():Bool {
		return _musicMuted;
	}
	public function setMusicMuted(value:Bool):Bool {
		if (_musicMuted != value) {
			_musicMuted = value;
			if (_musicSound != null)
				_musicSound.muted = _musicMuted;
			save();
		}
		return _musicMuted;
	}

	private function save():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		var data:PlayerData = so.data;
		data.soundMuted = _soundMuted;
		data.musicMuted = _musicMuted;
		so.flush();
	}

	private function load():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		var data:PlayerData = so.data;
		_soundMuted = data.soundMuted;
		_musicMuted = data.musicMuted;
	}

	public function clear():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		so.clear();
		so.flush();
	}

	private var _musicOn:Bool;
	private var _musicPosition:Float;

	public function setMusic(musicOn:Bool):Void {
		if (_musicOn != musicOn) {
			if (_musicSound != null) {
				_musicPosition = _musicSound.channel.position;
			}
			_musicOn = musicOn;
			if (_musicOn) {
				var startTime = !Math.isNaN(_musicPosition) ? _musicPosition : .0;
				playMusic(UISounds.hexa25, .5, startTime);
			} else {
				stopMusic();
			}
		}
	}

	public function connectTo(source:IEventDispatcher):Void {
		source.addEventListener(SoundEvent.PRESS, onSoundPress);
		source.addEventListener(SoundEvent.STAR_0, onStart0);
		source.addEventListener(SoundEvent.STAR_1, onStart1);
		source.addEventListener(SoundEvent.STAR_2, onStart2);
		source.addEventListener(SoundEvent.GAME_OVER, onGameOver);
		source.addEventListener(SoundEvent.RECORD, onRecord);
	}

	public function update():Void {
		_framePress = false;
	}

	private var _framePress:Bool;

	private function onSoundPress(event:SoundEvent):Void
	{
		if (!_framePress) {
			_framePress = true;
			playSound(UISounds.clap);
		}
	}

	private function onStart0(event:SoundEvent):Void {
		playSound(UISounds.star_1);
	}

	private function onStart1(event:SoundEvent):Void {
		playSound(UISounds.star_2);
	}

	private function onStart2(event:SoundEvent):Void {
		playSound(UISounds.star_3);
	}

	private function onGameOver(event:SoundEvent):Void {
		playSound(UISounds.game_over);
	}

	private function onRecord(event:SoundEvent):Void {
		playSound(UISounds.knock, 4.5);
	}
}
