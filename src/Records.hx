package ;

import flash.events.MouseEvent;
import flash.display.Shape;

class Records extends AScreen {
	public function new() {
		super();
	}

	private var _title:Shape;
	private var _menu:GameButton;
	private var _records:Array<RecordView>;

	override public function doOn():Void {
		_model.fix();

		_title = new Shape();
		UIFrames.records_normal.draw(_title.graphics, 0, 0);
		addChild(_title);

		_menu = new GameButton();
		_menu.image = UIFrames.button_image_menu;
		_menu.redraw();
		_menu.addEventListener(MouseEvent.MOUSE_DOWN, onMenuMouseDown);
		_menu.addEventListener(MouseEvent.CLICK, onMenuClick);
		addChild(_menu);

		_records = new Array<RecordView>();
		for (i in 0 ... _model.length()) {
			var score = _model.get(i);
			var view = new RecordView(score);
			view.redraw();
			addChild(view);
			_records.push(view);
		}
	}

	override public function doOff():Void {
	}

	override public function redraw():Void {
		_title.x = Std.int(w * .5);
		_title.y = 100;

		_menu.x = 5;
		_menu.y = h - _menu.h - 10;

		var recordH = 160;
		for (i in 0 ... _records.length) {
			var view = _records[i];
			view.x = Std.int(w * .5 - view.w * .5);
			view.y = Std.int(h * .7 - _records.length * recordH * .5 + i * recordH);
		}
	}

	private function onMenuClick(event:MouseEvent):Void {
		_main.setScreen(new Menu());
	}

	private function onMenuMouseDown(event:MouseEvent):Void {
		dispatchEvent(new SoundEvent(SoundEvent.PRESS));
	}
}
