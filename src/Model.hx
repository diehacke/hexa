package ;

import flash.Lib;
import flash.net.SharedObject;

typedef ModelInfo = {
	scores:Array<ScoreInfo>,
	lastScore:ScoreInfo,
	seed:Int
}

typedef ScoreInfo = {
	score:Int,
	remains:Int
}

class Model {
	inline private static var FILE_NAME:String = "hexa_scores.dat";
	inline private static var NUM_ITEMS:Int = 3;

	public function new() {
		_scores = new Array<Score>();
		load();
		fix();
	}

	public function fix():Void {
		if (_lastScore != null) {
			add(_lastScore);
			_changed = false;
		}
	}

	private var _scores:Array<Score>;
	private var _changed:Bool;

	inline public function length():Int {
		return _scores.length;
	}

	public function get(index:Int):Score {
		return _scores[index];
	}

	public function indexOf(score:Score):Int {
		return _scores.indexOf(score);
	}

	public function add(score:Score):Void {
		if (_lastScore == score)
			_lastScore = null;
		if (_scores.length == NUM_ITEMS) {
			var last = _scores[_scores.length - 1];
			if (last.moreThan(score))
				return;
		}
		if (_scores.length + 1 > NUM_ITEMS) {
			_scores.pop();
		}
		_scores.insert(findIndex(score), score);
		_changed = true;
	}

	private var _lastScore:Score;

	public function setLastScore(value:Score):Void {
		if (_lastScore != value) {
			_lastScore = value;
			_changed = true;
		}
	}

	private var _seed:Int;
	public function seed():Int {
		return _seed;
	}
	public function setSeed(value:Int):Void {
		if (_seed != value) {
			_seed = value;
			_changed = true;
		}
	}

	private function findIndex(score:Score):Int {
		var index = 0;
		while (true) {
			if (index >= _scores.length)
				break;
			var scoreI = _scores[index];
			if (score.moreThan(scoreI))
				break;
			index++;
		}
		return index;
	}

	public function isRecord(score:Score):Bool {
		if (_scores.length == 0)
			return false;
		return score.moreThan(_scores[0]);
	}

	public function doUpdate():Void {
		if (_changed) {
			save();
			_changed = false;
		}
	}

	private function load():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		var model:ModelInfo = so.data;
		_scores = new Array<Score>();
		if (model.scores != null) {
			for (score in model.scores) {
				if (score != null) {
					_scores.push(new Score(score.score, score.remains));
				}
			}
		}
		_seed = model.seed;
		_lastScore = model.lastScore != null ? new Score(model.lastScore.score, model.lastScore.remains) : null;
	}

	private function save():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		so.clear();
		var model:ModelInfo = so.data;
		model.scores = new Array<ScoreInfo>();
		for (score in _scores) {
			model.scores.push({
				score:score.score(),
				remains:score.remains()
			});
		}
		model.lastScore = _lastScore != null ? {score:_lastScore.score(), remains:_lastScore.remains()} : null;
		model.seed = _seed;
		so.flush();
	}

	public function clear():Void {
		var so = SharedObject.getLocal(FILE_NAME);
		so.clear();
		so.flush();
	}
}
