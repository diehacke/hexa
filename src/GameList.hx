package ;

import flash.geom.Point;
import flash.display.Sprite;

class GameList extends UIWidget {
	private var _model:Model;
	private var _items:Array<GameOver>;

	public function new(model:Model) {
		super();

		_model = model;
		_model.fix();
		_items = new Array<GameOver>();
		var i = 0;
		while (i < _model.length()) {
			var score = _model.get(i);
			var item = new GameOver();
			item.owner = new Sprite();
			addChild(item.owner);
			item.owner.addChild(item);
			item.init(score);
			item.w = GameOver.WIDTH;
			item.h = GameOver.HEIGHT;
			item.redraw();
			_items.push(item);
			i++;
		}
		updateList();
	}

	public function add(item:GameOver):Void {
		if (_model.indexOf(item.score()) != -1)
			return;
		_model.add(item.score());
		var i = _items.length;
		while (i-- > 0) {
			var item = _items[i];
			var score = item.score();
			if (_model.indexOf(score) == -1) {
				_items.splice(i, 1);
				Tween.to(item, 200, {x:0, y:0, w:GameOver.WIDTH, h:0})
					.setEase(Tween.easeOut)
					.setVoidOnUpdate(item.redraw)
					.setOnComplete(onItemRemoveComplete);
			}
		}
		var index = _model.indexOf(item.score());
		if (index != -1) {
			item.owner = new Sprite();
			item.owner.x = getX(index);
			item.owner.y = getY(index);
			addChild(item.owner);
			var point = item.localToGlobal(new Point());
			var point = item.owner.globalToLocal(point);
			item.owner.addChild(item);
			item.x = point.x;
			item.y = point.y;
			_items.push(item);
			Tween.to(item, 300, {x:0, y:0, w:GameOver.WIDTH, h:GameOver.HEIGHT})
				.setEase(Tween.easeIn)
				.setVoidOnUpdate(item.redraw);
		} else {
			Tween.to(item, 300, {x:0, y:0, w:GameOver.WIDTH, h:GameOver.HEIGHT})
				.setEase(Tween.easeIn)
				.setVoidOnUpdate(item.redraw)
				.setOnComplete(onRemoveToEmptyComplete);
		}
		updateList();
	}

	private function updateList():Void {
		var i = 0;
		while (i < _model.length()) {
			var score = _model.get(i);
			var item = null;
			for (itemI in _items) {
				if (itemI.score() == score) {
					item = itemI;
					break;
				}
			}
			Tween.to(item.owner, 200, {x:getX(i), y:getY(i)})
				.setEase(Tween.easeOut);
			addChildAt(item.owner, 0);
			i++;
		}
	}

	private function getX(index:Int):Float {
		return -5 - index * 5;
	}

	private function getY(index:Int):Float {
		return index * (GameOver.HEIGHT - 2);
	}

	private function onItemRemoveComplete(tween:Tween<GameOver>):Void {
		if (tween.target().owner.parent != null)
			tween.target().owner.parent.removeChild(tween.target().owner);
	}

	private function onRemoveToEmptyComplete(tween:Tween<GameOver>):Void {
		if (tween.target().parent != null)
			tween.target().parent.removeChild(tween.target());
	}

	public function doUpdate():Void {
		_model.doUpdate();
	}

	public function setLastScore(value:Score):Void {
		_model.setLastScore(value);
	}

	public function seed():Int {
		return _model.seed();
	}
	public function setSeed(value:Int):Void {
		_model.setSeed(value);
	}

	public function isRecord(score:Score):Bool {
		return _model.isRecord(score);
	}
}
