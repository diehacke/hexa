package ;

import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.ColorTransform;

class SoundButton extends UIButton {
	private var _isMusic:Bool;

	public function new(isMusic:Bool) {
		_isMusic = isMusic;
		super();
		w = 36;
		h = 36;
	}

	override public function redraw():Void {
		var g = graphics;
		g.clear();
		var normal = _isMusic ? UIFrames.sound_button_2_normal : UIFrames.sound_button_1_normal;
		var over = _isMusic ? UIFrames.sound_button_2_over : UIFrames.sound_button_1_over;
		var selected = _isMusic ? UIFrames.sound_button_2_selected : UIFrames.sound_button_1_selected;
		var selectedOver = _isMusic ? UIFrames.sound_button_2_selected_over : UIFrames.sound_button_1_selected_over;
		if (_selected) {
			if (_isOver)
				selectedOver.drawTrimmed(g, 0, 0);
			else
				selected.drawTrimmed(g, 0, 0);
		} else {
			if (_isOver)
				over.drawTrimmed(g, 0, 0);
			else
				normal.drawTrimmed(g, 0, 0);
		}
	}
}
