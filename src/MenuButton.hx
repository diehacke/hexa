package ;

import flash.display.Shape;
import flash.geom.ColorTransform;
import flash.events.MouseEvent;
import flash.text.TextField;

class MenuButton extends UIButton {
	private var _image:Shape;
	private var _tf:TextField;

	public function new() {
		super();

		_tf = new UIFormat("Tahoma", 18, 0xffffff).setBold(true).newAutoSized();
		addChild(_tf);

		addEventListener(MouseEvent.MOUSE_DOWN, soundOnMouseDown);
	}

	inline public function text():String {
		return _tf.text;
	}
	public function setText(value:String):Void {
		_tf.text = value;
	}

	override public function redraw():Void {
		w = 200;
		h = 80;

		_tf.x = Std.int(w * .5 - _tf.width * .5);
		_tf.y = Std.int(h * .5 - _tf.height * .5);

		var g = graphics;
		g.clear();
		if (_enabled) {
			if (_isDown) {
				UIFrames.button_down.drawSliced(g, 0, 0, w, h, 20, 20, 20, 20);
				_tf.x += 2;
				_tf.y += 2;
			} else if (_isOver) {
				UIFrames.button_over.drawSliced(g, 0, 0, w, h, 20, 20, 20, 20);
			} else {
				UIFrames.button_up.drawSliced(g, 0, 0, w, h, 20, 20, 20, 20);
			}
		} else {
			UIFrames.button_disabled.drawSliced(g, 0, 0, w, h, 20, 20, 20, 20);
		}
	}

	private function soundOnMouseDown(event:MouseEvent):Void {
		dispatchEvent(new SoundEvent(SoundEvent.PRESS));
	}
}
