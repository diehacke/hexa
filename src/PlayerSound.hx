package ;

import flash.events.Event;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

class PlayerSound {
	private var _onComplete:PlayerSound->Void;

	private var _channel:SoundChannel;
	public var channel(get, never):SoundChannel;
	private function get_channel():SoundChannel {
		return _channel;
	}

	private var _sound:Sound;

	public function new(sound:Sound, startTime:Float, loops:Int, onComplete:PlayerSound->Void) {
		_sound = sound;
		_channel = _sound.play(startTime, loops);
		_volume = 1;
		_onComplete = onComplete;
		if (_onComplete != null)
			_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
	}

	public function play(startTime:Float, loops:Int):Void {
		_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
		_channel = _sound.play(startTime, loops);
		_channel.soundTransform = new SoundTransform(_muted ? 0 : _volume);
		if (_onComplete != null)
			_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
	}

	private function onSoundComplete(event:Event):Void {
		_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
		if (_onComplete != null)
			_onComplete(this);
	}

	private var _volume:Float;
	public var volume(get, set):Float;
	private function get_volume():Float {
		return _volume;
	}
	private function set_volume(value:Float):Float {
		if (_volume != value) {
			_volume = value;
			if (!_muted)
				_channel.soundTransform = new SoundTransform(_volume);
		}
		return _volume;
	}

	private var _muted:Bool;
	public var muted(get, set):Bool;
	private function get_muted():Bool {
		return _muted;
	}
	private function set_muted(value:Bool):Bool {
		if (_muted != value) {
			_muted = value;
			_channel.soundTransform = new SoundTransform(_muted ? 0 : _volume);
		}
		return _muted;
	}
}

