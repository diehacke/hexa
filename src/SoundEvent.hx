package ;

import flash.events.Event;

class SoundEvent extends Event {
	public static var PRESS:String = "SoundEvent.PRESS";
	public static var STAR_0:String = "SoundEvent.STAR_0";
	public static var STAR_1:String = "SoundEvent.STAR_1";
	public static var STAR_2:String = "SoundEvent.STAR_2";
	public static var GAME_OVER:String = "SoundEvent.GAME_OVER";
	public static var RECORD:String = "SoundEvent.RECORD";

	public function new(type:String) {
		super(type, true);
	}

	override public function clone():Event {
		return new SoundEvent(type);
	}
}
