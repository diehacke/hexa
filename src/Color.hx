package ;

class Color {
	public static var all:Array<Color> = new Array<Color>();

	public static var RED:Color = add("red", 0xff0000, UIMovies.bubble_red);
	public static var GREEN:Color = add("green", 0x008000, UIMovies.bubble_green);
	public static var BLUE:Color = add("blue", 0x0080ff, UIMovies.bubble_blue);
	public static var YELLOW:Color = add("yellow", 0xee8000, UIMovies.bubble_yellow);
	public static var PURPLE:Color = add("purple", 0x8030dd, UIMovies.bubble_purple);

	public static var BLACK:Color = {
		var color = new Color();
		color._name = "black";
		color._value = 0x803000;
		color;
	}

	private static function add(name:String, value:UInt, movie:Array<UIFrame>):Color {
		var color = new Color();
		color._name = name;
		color._value = value;
		color._movie = movie;
		all.push(color);
		return color;
	}

	private var _name:String;

	private function new() {
	}

	private var _value:Int;
	inline public function value():Int {
		return _value;
	}

	private var _frame:UIFrame;
	inline public function frame():UIFrame {
		return _frame;
	}

	private var _movie:Array<UIFrame>;
	inline public function movie():Array<UIFrame> {
		return _movie;
	}

	public function toString():String {
		return _name;
	}
}
