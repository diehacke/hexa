package ;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.filters.GlowFilter;
import elm.utils.Rand;

class Game extends AScreen {
	private var _newGame:Bool;

	public function new(newGame:Bool) {
		super();
		_newGame = newGame;
	}

	private var _field:GameField;
	private var _buttonsContainer:Sprite;
	private var _restart:ConfirmButton;
	private var _new:ConfirmButton;
	private var _menu:ConfirmButton;
	private var _scorePanel:ScorePanel;
	private var _fieldContainer:Sprite;
	private var _fieldSubcontainer:Sprite;
	private var _gameOverContainer:Sprite;
	private var _gameOver:GameOver;
	private var _gameList:GameList;

	private var _score:Int;

	override public function doOn():Void {
		_startFrame = -1;

		_gameList = new GameList(_model);
		addChild(_gameList);

		_fieldContainer = new Sprite();
		addChild(_fieldContainer);

		_buttonsContainer = new Sprite();
		_buttonsContainer.addEventListener(MouseEvent.ROLL_OVER, onButtonsRollOver);
		_buttonsContainer.addEventListener(MouseEvent.ROLL_OUT, onButtonsRollOut);
		addChild(_buttonsContainer);

		_restart = new ConfirmButton(UIFrames.button_image_restart);
		_restart.redraw();
		_restart.confirmed.addVoid(onRestartConfirmed);
		_restart.setEnabledImmediate(false);
		_buttonsContainer.addChild(_restart);

		_new = new ConfirmButton(UIFrames.button_image_new);
		_new.redraw();
		_new.confirmed.addVoid(onNewConfirmed);
		_buttonsContainer.addChild(_new);

		_menu = new ConfirmButton(UIFrames.button_image_menu);
		_menu.redraw();
		_menu.confirmed.addVoid(onMenuConfirmed);
		_buttonsContainer.addChild(_menu);

		_gameOverContainer = new Sprite();
		addChild(_gameOverContainer);

		if (_newGame) {
			start(getRandomSeed());
		} else {
			start(_gameList.seed());
		}
	}

	private function onButtonsRollOver(event:MouseEvent):Void {
		_restart.setAllowSelected(false);
		_new.setAllowSelected(false);
		_menu.setAllowSelected(false);
	}

	private function onButtonsRollOut(event:MouseEvent):Void {
		_restart.setAllowSelected(true);
		_new.setAllowSelected(true);
		_menu.setAllowSelected(true);
	}

	override public function redraw():Void
	{
		var buttonH = 90;

		_restart.x = 5;
		_restart.y = h - buttonH * 3;

		_new.x = 5;
		_new.y = h - buttonH * 2;

		_menu.x = 5;
		_menu.y = h - buttonH;

		_gameOverContainer.x = Std.int(w * .5);
		_gameOverContainer.y = Std.int(h * .5);

		_gameList.y = 50;
	}

	private function arrangeScorePanel()
	{
		if (_scorePanel != null) {
			_scorePanel.x = 12;
		}
	}

	private function arrangeBubble(bubble:Bubble, hasDelay:Bool):Void {
		if (bubble.cell() != null) {
			bubble.view.pushMotion(bubble.cell().getX(), bubble.cell().getY(), hasDelay);
		}
	}

	private function removeBubble(bubble:Bubble):Void {
		if (bubble.cell() != null) {
			bubble.view.explode();
			bubble.cell().setBubble(null);
		}
	}

	private function onBubbleMouseDown(view:BubbleView):Void {
		var cell = view.bubble().cell();
		if (cell == null)
			return;
		if (hasColoredNeighbours(cell)) {
			var needCheckTurns = false;
			var color = cell.bubble().color();
			var counter = new Counter();
			removeColoredNeighbours(cell, color, counter);
			if (counter.getScore() > 0)
			{
				_score += counter.getScore();
				var hint = new ScoreHint(color, counter.getScore() + "");
				hint.x = mouseX;
				hint.y = mouseY - 30;
				hint.flow();
				addChild(hint);
				_scorePanel.setScore(_score, true);
				needCheckTurns = true;
			}
			var hasDelay = fallDown();
			var falledRight = fallRightBottom(hasDelay);
			if (needCheckTurns) {
				_changes++;
				checkTurns();
			}
			_player.playSound(UISounds.glass);
			Tween.to(new TweenKey(), 50, null).setVoidOnComplete(playKnock);
			if (hasDelay && falledRight)
				Tween.to(new TweenKey(), 350, null).setVoidOnComplete(playKnock);
		}
	}

	private function playKnock():Void {
		_player.playSound(UISounds.knock);
	}

	private function hasColoredNeighbours(cell:Cell):Bool {
		var color = cell.bubble().color();
		for (cellI in cell.neighbours()) {
			if (cellI.bubble() != null && cellI.bubble().color() == color) {
				return true;
			}
		}
		return false;
	}

	private var _highlighted:Array<BubbleView>;

	private function onBubbleRollOver(view:BubbleView):Void {
		clearHighlighted();
		var cell = view.bubble().cell();
		if (cell == null)
			return;
		if (hasColoredNeighbours(cell)) {
			var color = cell.bubble().color();
			fillColoredNeighbours(_highlighted, cell, color);
			for (view in _highlighted) {
				view.setSelected(true);
			}
		}
	}

	private function onBubbleRollOut(view:BubbleView):Void {
		clearHighlighted();
	}

	private function clearHighlighted():Void {
		if (_highlighted != null) {
			for (view in _highlighted) {
				view.setSelected(false);
			}
		}
		_highlighted = new Array<BubbleView>();
	}

	private function fillColoredNeighbours(highlighted:Array<BubbleView>, cell:Cell, color:Color):Void {
		if (highlighted.indexOf(cell.bubble().view) != -1)
			return;
		highlighted.push(cell.bubble().view);
		for (cellI in cell.neighbours()) {
			if (cellI.bubble() != null && cellI.bubble().color() == color) {
				fillColoredNeighbours(highlighted, cellI, color);
			}
		}
	}
	
	private function removeColoredNeighbours(cell:Cell, color:Color, counter:Counter):Void {
		removeBubble(cell.bubble());
		counter.numRemoved++;
		for (cellI in cell.neighbours()) {
			if (cellI.bubble() != null && cellI.bubble().color() == color) {
				removeColoredNeighbours(cellI, color, counter);
			}
		}
	}

	private function fallDown():Bool {
		var changed = false;
		var bubblesToArrange = new Array<Bubble>();
		for (i in 0 ... _field.sizeI()) {
			var j = _field.sizeJ() - 1;
			while (j-- > 0) {
				var cell = _field.get(i, j);
				if (cell == null)
					continue;
				var bubble = cell.bubble();
				if (bubble != null) {
					for (jj in j + 1 ... _field.sizeJ()) {
						var cell = _field.get(i, jj);
						if (cell == null)
							continue;
						if (cell.bubble() != null)
							break;
						cell.setBubble(bubble);
						if (bubblesToArrange.indexOf(bubble) == -1)
							bubblesToArrange.push(bubble);
						changed = true;
					}
				}
			}
		}
		for (bubble in bubblesToArrange) {
			arrangeBubble(bubble, false);
		}
		return changed;
	}

	private function fallRightBottom(hasDelay:Bool):Bool {
		var bubblesToArrange = new Array<Bubble>();
		var i = _field.sizeI();
		while (i-- > 0) {
			var j = _field.sizeJ();
			while (j-- > 0) {
				var cell = _field.get(i, j);
				if (cell == null)
					continue;
				var bubble = cell.bubble();
				if (bubble != null) {
					while (true) {
						cell = cell.rightBottom();
						if (cell == null || cell.bubble() != null)
							break;
						cell.setBubble(bubble);
						if (bubblesToArrange.indexOf(bubble) == -1)
							bubblesToArrange.push(bubble);
					}
				}
			}
		}
		for (bubble in bubblesToArrange) {
			arrangeBubble(bubble, hasDelay);
		}
		return bubblesToArrange.length > 0;
	}

	private function isFree(i:Int):Bool {
		for (j in 0 ... _field.sizeJ()) {
			if (_field.get(i, j).bubble() != null)
				return false;
		}
		return true;
	}

	override public function doOnEnterFrame():Void {
		if (_startFrame != -1) {
			for (i in 0 ... _field.sizeI()) {
				for (j in 0 ... _field.sizeJ()) {
					var cell = _field.get(i, j);
					if (cell == null)
						continue;
					var bubble = cell.bubble();
					if (bubble != null && bubble.view.riseFrame != -1) {
						bubble.view.riseFrame++;
						if (bubble.view.riseFrame > 4)
							bubble.view.riseFrame = -1;
						bubble.view.redraw();
					}
				}
			}
			_startFrame++;
			if (_startFrame > 8) {
				_startFrame = -1;
			}
		}
		for (i in 0 ... _field.sizeI()) {
			for (j in 0 ... _field.sizeJ()) {
				var cell = _field.get(i, j);
				if (cell == null)
					continue;
				var bubble = cell.bubble();
				if (bubble != null) {
					bubble.view.updateMotions();
				}
			}
		}
		var g = graphics;
		g.clear();
		if (_highlighted != null && _highlighted.length > 0) {
			g.lineStyle(BubbleView.SIZE + 2, _highlighted[0].bubble().color().value());
			for (view in _highlighted) {
				var cell = view.bubble().cell();
				if (cell != null) {
					for (cellI in cell.neighbours()) {
						if (cellI.bubble() != null && cellI.bubble().color() == cell.bubble().color()) {
							g.moveTo(cell.bubble().view.x, cell.bubble().view.y);
							g.lineTo(cellI.bubble().view.x, cellI.bubble().view.y);
						}
					}
				}
			}
		}
		_gameList.doUpdate();
	}

	private function onRestartConfirmed():Void {
		start(_seed);
	}

	private function onNewConfirmed():Void {
		start(getRandomSeed());
	}

	private function onMenuConfirmed():Void {
		_main.setScreen(new Menu());
	}

	private var _seed:Int;
	private var _hasTurns:Bool;
	private var _changes:Int;

	private function start(seed:Int):Void {
		var oldContainer = _fieldSubcontainer;
		_fieldSubcontainer = new Sprite();
		_fieldContainer.addChild(_fieldSubcontainer);

		if (_gameOver != null) {
			_gameList.add(_gameOver);
			addChild(_gameList);
			_gameOver = null;
		}
		if (_scorePanel == null) {
			_scorePanel = new ScorePanel();
			_scorePanel.redraw();
			addChild(_scorePanel);
			arrangeScorePanel();
		}

		_seed = seed;
		_score = 0;
		_changes = 0;
		Rand.instance.seed = _seed;
		var oldField = _field;
		_field = new GameField(16, 16);
		for (i in 0 ... _field.sizeI()) {
			for (j in 0 ... _field.sizeJ()) {
				var cell = _field.get(i, j);
				if (cell == null)
					continue;
				var index = Rand.instance.getInt(0, Color.all.length - 1);
				var bubble = new Bubble(Color.all[index]);
				cell.setBubble(bubble);
				bubble.view = new BubbleView(bubble);
				bubble.view.mouseDown.add(onBubbleMouseDown);
				bubble.view.rollOver.add(onBubbleRollOver);
				bubble.view.rollOut.add(onBubbleRollOut);
				bubble.view.redraw();
				bubble.view.immediateMotion(cell.getX(), cell.getY());
				bubble.view.updateMotions();
				var color = null;
				if (oldField != null) {
					var oldCell = oldField.get(i, j);
					if (oldCell != null && oldCell.bubble() != null) {
						color = oldCell.bubble().color();
						if (color != null && color == bubble.color()) {
							oldContainer.removeChild(oldCell.bubble().view);
						}
					}
				}
				bubble.view.riseFrame = color != null && color == bubble.color() ? -1 : Math.round(Math.random() * 2);
				_fieldSubcontainer.addChild(bubble.view);
			}
		}
		_scorePanel.setScore(_score, false);
		checkTurns();
		_startFrame = 0;

		if (oldContainer != null) {
			Tween.to(oldContainer, 200, {alpha:0}).setOnComplete(onFieldRemoveComplete);
		}

		_gameList.setSeed(_seed);
		_player.setMusic(true);
	}

	private function onFieldRemoveComplete(tween:Tween<Sprite>):Void {
		if (tween.target().parent != null)
			tween.target().parent.removeChild(tween.target());
	}

	private var _startFrame:Int;
	
	private function getRandomSeed():Int {
		return Std.int(Math.random() * 1000);
	}

	private function checkTurns():Void {
		_hasTurns = hasTurns();
		if (!_hasTurns) {
			if (_gameOver == null) {
				_gameOver = new GameOver();
				_gameOver.setCustomAlpha(0);
				_gameOver.x = Std.int(-_gameOver.w * .5);
				_gameOver.y = Std.int(-_gameOver.h * .5 - 100);
				addChild(_gameOverContainer);
				_gameOverContainer.addChild(_gameOver);
				_gameOver.redraw();

				var remains = getNumBubbles();
				_gameOver.show(_gameList, _scorePanel, _score, remains);
				_scorePanel = null;

				Tween.to(_gameOver, 400, {y:Std.int(-_gameOver.h * .5)})
					.setEase(Tween.easeOutBounce)
					.setOnUpdate(onGameOverUpdate);
			}
			_player.setMusic(false);
		}
		_restart.needConfirm = _hasTurns && _changes > 1;
		_restart.setEnabled(_changes > 0);
		_new.needConfirm = _hasTurns && _changes > 0;
		_new.setSelected(!_hasTurns);
		_menu.needConfirm = _hasTurns && _changes > 0;
	}

	private function onGameOverUpdate(tween:Tween<GameOver>):Void {
		tween.target().setCustomAlpha(tween.value(0, 1));
	}

	private function hasTurns():Bool {
		for (i in 0 ... _field.sizeI()) {
			for (j in 0 ... _field.sizeJ()) {
				var cell = _field.get(i, j);
				if (cell == null)
					continue;
				var bubble = cell.bubble();
				if (bubble == null)
					continue;
				var color = bubble.color();
				for (cellI in cell.neighbours()) {
					if (cellI.bubble() != null && cellI.bubble().color() == color)
						return true;
				}
			}
		}
		return false;
	}

	private function getNumBubbles():Int {
		var result = 0;
		for (i in 0 ... _field.sizeI()) {
			for (j in 0 ... _field.sizeJ()) {
				var cell = _field.get(i, j);
				if (cell != null && cell.bubble() != null)
					result++;
			}
		}
		return result;
	}

	private var _debugButton:GameButton;
	private var _debugButton2:GameButton;

	override public function doOnDebugPress():Void {
		if (_debugButton == null) {
			_debugButton = new GameButton();
			_debugButton.redraw();
			_debugButton.x = h - 90;
			_debugButton.y = w - 90;
			_debugButton.addEventListener(MouseEvent.CLICK, onDebugButtonClick);
			addChild(_debugButton);

			_debugButton2 = new GameButton();
			_debugButton2.redraw();
			_debugButton2.x = h - 180;
			_debugButton2.y = w - 90;
			_debugButton2.addEventListener(MouseEvent.CLICK, onDebugButton2Click);
			addChild(_debugButton2);
		} else {
			removeChild(_debugButton);
			_debugButton = null;
			removeChild(_debugButton2);
			_debugButton2 = null;
		}
	}

	private function onDebugButtonClick(event:MouseEvent):Void {
		while (_hasTurns) {
			for (i in 0 ... _field.sizeI()) {
				for (j in 0 ... _field.sizeJ()) {
					var cell = _field.get(i, j);
					if (cell == null)
						continue;
					var bubble = cell.bubble();
					if (bubble == null)
						continue;
					if (bubble.view != null)
						bubble.view.mouseDown.dispatch(bubble.view);
				}
			}
		}
	}

	private function onDebugButton2Click(event:MouseEvent):Void {
		_model.clear();
		_player.clear();
		_main.setScreen(new Menu());
	}
}
