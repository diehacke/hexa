package ;

import flash.text.TextField;
import flash.geom.ColorTransform;

class ThinButton extends UIButton {
	private var _normalFormat:UIFormat;
	private var _selectedFormat:UIFormat;
	private var _tf:TextField;

	public function new() {
		super();

		_normalFormat = new UIFormat("Tahoma", 18, 0x5F3930).setBold(true);
		_selectedFormat = new UIFormat("Tahoma", 18, 0xffffff).setBold(true);
		_tf = _normalFormat.newAutoSized();
		addChild(_tf);
	}

	inline public function text():String {
		return _tf.text;
	}
	public function setText(value:String):Void {
		_tf.text = value;
	}

	override public function redraw():Void {
		w = 80;
		h = 40;

		_tf.x = Std.int(w * .5 - _tf.width * .5);
		_tf.y = Std.int(h * .5 - _tf.height * .5);

		var g = graphics;
		g.clear();
		if (_selected) {
			if (_isDown) {
				UIFrames.thin_button_selected_down.draw(g, 0, 0);
				_tf.x += 2;
				_tf.y += 2;
			} else if (_isOver) {
				UIFrames.thin_button_selected_over.draw(g, 0, 0);
			} else {
				UIFrames.thin_button_selected_up.draw(g, 0, 0);
			}
		} else {
			if (_isDown) {
				UIFrames.thin_button_down.draw(g, 0, 0);
				_tf.x += 2;
				_tf.y += 2;
			} else if (_isOver) {
				UIFrames.thin_button_over.draw(g, 0, 0);
			} else {
				UIFrames.thin_button_up.draw(g, 0, 0);
			}
		}

		if (_selected) {
			_selectedFormat.applyTo(_tf);
		} else {
			_normalFormat.applyTo(_tf);
		}
		_tf.transform.colorTransform = _isOver ? new ColorTransform(1.1, 1.1, 1.1) : new ColorTransform();
	}
}
