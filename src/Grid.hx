package ;

class Grid<T> {
	private var _bubbles:Array<T>;

	public function new(sizeI:Int, sizeJ:Int) {
		_sizeI = sizeI;
		_sizeJ = sizeJ;
		_bubbles = new Array<T>();
	}

	private var _sizeI:Int;
	inline public function sizeI():Int {
		return _sizeI;
	}

	private var _sizeJ:Int;
	inline public function sizeJ():Int {
		return _sizeJ;
	}

	inline private function isCorrect(i:Int, j:Int):Bool {
		return i >= 0 && i < _sizeI && j >= 0 && j < _sizeJ;
	}

	public function get(i:Int, j:Int):T {
		return isCorrect(i, j) ? _bubbles[i * _sizeJ + j] : null;
	}

	public function set(i:Int, j:Int, value:T):Void {
		if (isCorrect(i, j))
			_bubbles[i * _sizeJ + j] = value;
	}
}
