package ;

import flash.text.TextField;
import flash.filters.GlowFilter;

class ScorePanel extends UIWidget {
	private var _scoreTf:TextField;

	public function new() {
		super();

		_scoreTf = new UIFormat("Tahoma", 40, 0xffffff)
			.setBold(true)
			.addFilter(new GlowFilter(0x803000))
			.newAutoSized(false);
		_scoreTf.text;
		addChild(_scoreTf);
	}

	public function setScore(value:Int, jump:Bool):Void {
		_scoreTf.text = separate(value + "");
		w = _scoreTf.width;
		h = _scoreTf.height;
		if (jump) {
			var scale = 1.2;
			var vars = {
				scaleX:scale,
				scaleY:scale,
				x:_scoreTf.width * (1 - scale) * .5,
				y:_scoreTf.height * (1 - scale) * .5,
			};
			Tween.to(_scoreTf, 100, vars)
				.setEase(Tween.easeOut)
				.setVoidOnComplete(onJumpTweenComplete);
		}
	}

	private function onJumpTweenComplete():Void {
		var vars = {scaleX:1, scaleY:1, x:0, y:0};
		Tween.to(_scoreTf, 200, vars)
			.setEase(Tween.easeOut);
	}

	private function separate(value:String):String {
		if (value.length <= 3)
			return value;
		return separate(value.substr(0, value.length - 3)) + " " + value.substr(value.length - 3);
	}
}
