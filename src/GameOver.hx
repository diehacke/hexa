package ;

import flash.geom.Point;
import flash.geom.ColorTransform;
import flash.text.TextField;
import flash.display.Shape;
import flash.display.Sprite;
import flash.filters.GlowFilter;

class GameOver extends UIWidget {
	public static var WIDTH:Int = 120;
	public static var HEIGHT:Int = 30;

	private var _remainsTfContainer:Sprite;
	private var _remainsTf:TextField;
	private var _statusContainer:Sprite;
	private var _status:TextField;
	private var _scorePanelContainer:Sprite;
	private var _scorePanelSubcontainer:Sprite;
	private var _scorePanel:ScorePanel;
	private var _smallScore:TextField;
	private var _smallRemains:TextField;
	private var _stars:Array<Star>;
	private var _recordContainer:Sprite;
	private var _record:Shape;

	public function new() {
		super();

		mouseEnabled = false;
		mouseChildren = false;

		_customAlpha = 1;

		_remainsTfContainer = new Sprite();
		addChild(_remainsTfContainer);

		_remainsTf = new UIFormat("Tahoma", 30, 0x5F3930)
			.setBold(true)
			.newAutoSized(false);
		_remainsTfContainer.addChild(_remainsTf);

		_statusContainer = new Sprite();
		addChild(_statusContainer);

		_status = new UIFormat("Tahoma", 30, 0x5F3930)
			.setBold(true)
			.newAutoSized(false);
		_statusContainer.addChild(_status);

		_smallScore = new UIFormat("Tahoma", 16, 0x5F3930).setBold(true).newAutoSized();
		addChild(_smallScore);

		_smallRemains = new UIFormat("Tahoma", 14, 0x5F3930).setBold(true).newAutoSized();
		addChild(_smallRemains);

		_stars = new Array<Star>();
		for (i in 0 ... 3) {
			var star = new Star(i);
			star.redraw();
			addChild(star);
			_stars[i] = star;
		}

		_scorePanelContainer = new Sprite();
		addChild(_scorePanelContainer);

		_scorePanelSubcontainer = new Sprite();
		_scorePanelContainer.addChild(_scorePanelSubcontainer);

		_recordContainer = new Sprite();
		addChild(_recordContainer);

		w = 400;
		h = 200;
	}

	public var owner:Sprite;

	override public function redraw():Void {
		var g = graphics;
		g.clear();
		if (h > 20)
			UIFrames.game_over_bg.drawSliced(g, 0, 0, w + 4, h + 4, 10, 10, 10, 10);

		for (i in 0 ... 3) {
			var star = _stars[i];
			var scale = MathUtil.clamp((w - 180) / 200, 0, 1);
			star.x = Std.int(w * .5 + (i - 1) * w * .32);
			star.scaleX = scale;
			star.scaleY = scale;
			star.visible = w > 200;
		}

		if (w < 300) {
			var ratio = MathUtil.clamp(1 - (w - 100) / 200, 0, 1);
			_smallScore.visible = ratio > .3 && h > 20;
			_smallScore.x = w * .2;
			_smallScore.y = h * .5 - _smallScore.height * .5;

			_smallRemains.visible = ratio > .7 && h > 20;
			_smallRemains.x = _smallScore.x + _smallScore.width + 2;
			_smallRemains.y = h * .5 - _smallRemains.height * .5;
		} else {
			_smallScore.visible = false;
			_smallRemains.visible = false;
		}

		_scorePanelContainer.visible = w > 200;
		_scorePanelContainer.x = w * .5;
		_scorePanelContainer.y = h * .4;

		_remainsTfContainer.alpha = _customAlpha;
		_remainsTfContainer.x = w * .5;
		_remainsTfContainer.y = h * (_status.text != "" ? .6 : .7);
		_remainsTfContainer.visible = w > 200;

		_statusContainer.alpha = _customAlpha;
		_statusContainer.visible = w > 200;
		_statusContainer.x = w * .5;
		_statusContainer.y = h * .8;

		_recordContainer.visible = w > 200;
		_recordContainer.x = w * .5;
		_recordContainer.y = h * .5;
	}

	private var _score:Score;
	inline public function score():Score {
		return _score;
	}

	private var _bonus:Int;

	public function init(score:Score):Void {
		_score = score;
		_smallScore.text = _score.score() + "";
		_smallRemains.text = "/ " + _score.remains();
	}

	public function show(gameList:GameList, scorePanel:ScorePanel, score:Int, remains:Int):Void {
		_remainsTf.text = "Remains: " + remains;
		_status.text = remains == 0 ? "Pure win!" : "";

		_bonus = Counter.getBonus(remains);
		if (_bonus > 0) {
			score += _bonus;
		}
		_score = new Score(score, remains);
		gameList.setLastScore(_score);

		_scorePanel = scorePanel;
		var offset = new Point(_scorePanel.w * .5, _scorePanel.h * .5);
		var point = _scorePanel.localToGlobal(offset);
		var point = _scorePanelContainer.globalToLocal(point);
		_scorePanelSubcontainer.addChild(_scorePanel);
		_scorePanelSubcontainer.x = point.x;
		_scorePanelSubcontainer.y = point.y;
		_scorePanel.x = -offset.x;
		_scorePanel.y = -offset.y;
		Tween.to(_scorePanelSubcontainer, 500, {x:0, y:0}).setEase(Tween.easeOutBounce);
		_smallScore.text = _score.score() + "";
		_smallRemains.text = "/ " + _score.remains();
		redraw();

		if (_bonus > 0) {
			Tween.to(new TweenKey(), 500, null).setVoidOnComplete(onBonusShow);
		}

		var stars = Counter.getStars(_score.score(), _score.remains());
		for (i in 0 ... 3) {
			var star = _stars[i];
			if (i < stars) {
				star.show((_bonus > 0 ? 500 : 30) + i * 500);
			}
		}

		var scale = 2;
		var vars = {
			scaleX:1,
			scaleY:1,
			x:-_remainsTf.width * .5,
			y:-_remainsTf.height * .5,
			alpha:1
		};
		_remainsTf.x = -_remainsTf.width * .5 * scale;
		_remainsTf.y = -_remainsTf.height * .5 * scale - 30;
		_remainsTf.scaleX = scale;
		_remainsTf.scaleY = scale;
		_remainsTf.alpha = 0;
		Tween.to(_remainsTf, 200, vars)
			.setWait(_bonus > 0 ? 600 : 200)
			.setEase(Tween.easeIn);

		if (_status.text != "") {
			var scale = 2;
			var vars = {
				scaleX:1,
				scaleY:1,
				x:-_status.width * .5,
				y:-_status.height * .5,
				alpha:1
			};
			_status.x = -_status.width * .5 * scale;
			_status.y = -_status.height * .5 * scale - 30;
			_status.scaleX = scale;
			_status.scaleY = scale;
			_status.alpha = 0;
			Tween.to(_status, 200, vars)
				.setWait(_bonus > 0 ? 100 : 600)
				.setEase(Tween.easeIn);
		}

		if (gameList.isRecord(_score)) {
			_record = new Shape();
			UIFrames.record_normal.draw(_record.graphics, 0, 0);
			_recordContainer.addChild(_record);
			var scale = 2;
			var vars = {
				scaleX:1,
				scaleY:1,
				x:100,
				y:10,
				alpha:1
			};
			_record.x = 72;
			_record.y = 100;
			_record.scaleX = scale;
			_record.scaleY = scale;
			_record.alpha = 0;
			var wait = _bonus > 0 ? 100 : 700;
			Tween.to(_record, 150, vars)
				.setWait(wait)
				.setEase(Tween.easeIn)
				.setVoidOnComplete(playRecordSound);
			Tween.to(_record, 150, null, new TweenKey())
				.setWait(wait + 100)
				.setEase(Tween.linear)
				.setOnUpdate(onRecordUpdate);
		}

		dispatchEvent(new SoundEvent(SoundEvent.GAME_OVER));
	}

	private function playRecordSound():Void {
		dispatchEvent(new SoundEvent(SoundEvent.RECORD));
	}

	private function onRecordUpdate(tween:Tween<Shape>) {
		var value = tween.value(0, 1);
		var t = Bezier.instance.xs(0, .5, 1).ys(1, 2, 1).y(value);
		_record.transform.colorTransform = new ColorTransform(t, t, t);
	}

	private function onBonusShow():Void {
		_scorePanel.setScore(_score.score(), true);
		var hint = new ScoreHint(Color.BLACK, "BONUS!\n" + _bonus);
		hint.x = w * .5;
		hint.y = h * .5;
		hint.flowBig();
		addChild(hint);
	}

	private var _customAlpha:Float;

	public function setCustomAlpha(value:Float):Void {
		_customAlpha = value;
		redraw();
	}
}
