package ;

import flash.geom.Point;
import flash.geom.ColorTransform;
import flash.text.TextField;
import flash.display.Shape;
import flash.display.Sprite;
import flash.filters.GlowFilter;

class RecordView extends UIWidget {
	private var _remainsTf:TextField;
	private var _scorePanel:ScorePanel;
	private var _stars:Array<Star>;

	public function new(score:Score) {
		super();

		_score = score;

		mouseEnabled = false;
		mouseChildren = false;

		_remainsTf = new UIFormat("Tahoma", 30, 0x5F3930)
			.setBold(true)
			.newAutoSized(false);
		_remainsTf.text = _score.remains() == 0 ? "Pure win!" : "Remains: " + _score.remains();
		addChild(_remainsTf);

		_stars = new Array<Star>();
		for (i in 0 ... 3) {
			var star = new Star(i);
			star.redraw();
			addChild(star);
			_stars[i] = star;
		}

		_scorePanel = new ScorePanel();
		_scorePanel.setScore(_score.score(), false);
		addChild(_scorePanel);

		var stars = Counter.getStars(score.score(), score.remains());
		for (i in 0 ... 3) {
			var star = _stars[i];
			if (i < stars) {
				star.showImmediate();
			}
		}
	}

	private var _score:Score;
	public function score():Score {
		return _score;
	}

	override public function redraw():Void {
		w = 400;
		h = 100;
		var g = graphics;
		g.clear();
		UIFrames.game_over_bg.drawSliced(g, 0, 0, w + 4, h + 4, 10, 10, 10, 10);
		for (i in 0 ... 3) {
			var star = _stars[i];
			var scale = MathUtil.clamp((w - 180) / 200, 0, 1);
			star.x = Std.int(w * .5 + (i - 1) * w * .32);
			star.scaleX = scale;
			star.scaleY = scale;
			star.visible = w > 200;
		}

		_scorePanel.x = 10;
		_scorePanel.y = h - _scorePanel.h;

		_remainsTf.x = w - _remainsTf.width - 10;
		_remainsTf.y = h - _remainsTf.height;
	}
}
