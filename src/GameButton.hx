package ;

import flash.display.Shape;
import flash.geom.ColorTransform;

class GameButton extends UIButton {
	private var _image:Shape;

	public function new() {
		super();

		_image = new Shape();
		addChild(_image);
	}

	public var image:UIFrame;
	public var onRedraw:Void->Void;

	override public function redraw():Void {
		w = 80;
		h = 80;

		var g = graphics;
		g.clear();
		if (_isDown) {
			UIFrames.button_down.draw(g, 0, 0);
		} else if (_isOver) {
			UIFrames.button_over.draw(g, 0, 0);
		} else {
			UIFrames.button_up.draw(g, 0, 0);
		}

		_image.transform.colorTransform = _isOver ? new ColorTransform(1.1, 1.1, 1.1) : new ColorTransform();
		var g = _image.graphics;
		g.clear();
		if (image != null) {
			var offset = _isDown ? 2. : .0;
			image.draw(g, w * .5 + offset, h * .5 + offset);
		}

		if (onRedraw != null)
			onRedraw();
	}
}
