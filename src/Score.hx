package ;

class Score {
	public function new(score:Int, remains:Int) {
		_score = score;
		_remains = remains;
	}

	private var _score:Int;
	inline public function score():Int {
		return _score;
	}

	private var _remains:Int;
	inline public function remains():Int {
		return _remains;
	}

	public function moreThan(score:Score):Bool {
		return _score > score.score() ||
			_score == score.score() && _remains < score.remains();
	}

	public function toString():String {
		return "(" + score + "/" + remains + ")";
	}
}
