package ;

import flash.text.TextField;
import flash.filters.GlowFilter;

class ScoreHint extends UIWidget {
	private var _tf:TextField;

	public function new(color:Color, value:String) {
		super();
		mouseEnabled = false;
		mouseChildren = false;
		_tf = new UIFormat("Tahoma", 36, 0xffffff)
			.setBold(true)
			.setCenter()
			.addFilter(new GlowFilter(color.value()))
			.newAutoSized(false);
		_tf.text = value;
		_tf.x = -_tf.width * .5;
		_tf.y = -_tf.height * .5;
		addChild(_tf);
	}

	public function flow():Void {
		var key = new TweenKey();
		Tween.to(this, 300, null, key).setOnUpdate(onTweenUpdate);
		Tween.to(this, 700, {y:y - 200, alpha:.0}).setVoidOnComplete(onTweenComplete);
	}

	public function flowBig():Void {
		var key = new TweenKey();
		scaleX = 5;
		scaleY = 5;
		Tween.to(this, 500, {scaleX:2, scaleY:2}, key)
			.setEase(Tween.easeOutBounce);
		Tween.to(this, 2000, {y:y - 200})
			.setEase(Tween.linear)
			.setOnUpdate(onTweenUpdateBigAlpha)
			.setVoidOnComplete(onTweenComplete);
	}

	private function onTweenUpdate(tween:Tween<ScoreHint>):Void {
		var value = Bezier.instance.xs(0, .3, 1).ys(1.5, 2, 1).y(tween.value(0, 1));
		scaleX = value;
		scaleY = value;
	}

	private function onTweenUpdateBigAlpha(tween:Tween<ScoreHint>):Void {
		if (tween.value(0, 1) < 1 / 4) {
			alpha = Bezier.instance.xs(0, .2, 1).ys(0, 1, 1).y(tween.value(0, 1) * 4);
		} else {
			alpha = Bezier.instance.xs(0, .5, 1).ys(1, .8, 0).y((tween.value(0, 1) - 1 / 4) / (3 / 4));
		}
	}

	private function onTweenComplete():Void {
		if (parent != null)
			parent.removeChild(this);
	}
}
