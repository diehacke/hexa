package ;

import flash.text.TextField;

class ConfirmPanel extends UIWidget {
	private var _yes:ThinButton;
	private var _no:ThinButton;
	private var _tf:TextField;

	public function new() {
		super();

		_tf = new UIFormat("Tahoma", 18, 0x5F3930).setBold(true).newAutoSized();
		_tf.text = "Are you shure?";
		addChild(_tf);

		_yes = new ThinButton();
		_yes.setText("YES");
		_yes.setSelected(true);
		_yes.redraw();
		addChild(_yes);

		_no = new ThinButton();
		_no.setText("NO");
		_no.redraw();
		addChild(_no);
	}

	inline public function yes():ThinButton {
		return _yes;
	}

	inline public function no():ThinButton {
		return _no;
	}

	override public function redraw():Void {
		w = 190;
		h = 78;

		var buttonsY = 38;

		_tf.x = Std.int(w - 80 - _tf.width * .5);
		_tf.y = Std.int(buttonsY * .5 - _tf.height * .5);

		_yes.x = w - 160;
		_yes.y = buttonsY;

		_no.x = w - 80;
		_no.y = buttonsY;

		var g = graphics;
		g.clear();
		UIFrames.confirmation_normal.draw(g, 0, 0);
	}
}
