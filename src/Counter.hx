package ;

class Counter {
	public function new() {
	}

	public var numRemoved:Int;

	public function getScore():Int {
		var value = numRemoved * 10;
		if (numRemoved > 2)
			value += (numRemoved - 2) * 5;
		if (numRemoved > 5)
			value += (numRemoved - 5) * 10;
		if (numRemoved > 10)
			value += (numRemoved - 5) * 20;
		return value;
	}

	public static function getBonus(remains:Int):Int {
		return (remains <= 3 ? 3 - remains : 0) * 100 + (remains == 0 ? 1 : 0) * 200;
	}

	public static function getStars(score:Int, remains:Int):Int {
		var stars = 0;
		if (score > 1500)
			stars++;
		if (score > 2500)
			stars++;
		if (remains == 0)
			stars++;
		return stars;
	}
}
