package ;

class GameField {
	private var _cells:Grid<Cell>;

	public function new(sizeI:Int, sizeJ:Int) {
		_cells = new Grid<Cell>(sizeI, sizeJ);
		for (i in 0 ... _cells.sizeI()) {
			for (j in 0 ... _cells.sizeJ()) {
				if (getK(i, j) > sizeI / 2 - 2 &&
					getL(i, j) > -sizeI / 2)
					_cells.set(i, j, new Cell(i, j));
			}
		}
		for (i in 0 ... _cells.sizeI()) {
			for (j in 0 ... _cells.sizeJ()) {
				var cell = _cells.get(i, j);
				if (cell == null)
					continue;
				cell.init(_cells);
			}
		}
	}

	inline public function sizeI():Int {
		return _cells.sizeI();
	}

	inline public function sizeJ():Int {
		return _cells.sizeJ();
	}

	public function get(i:Int, j:Int):Cell {
		return _cells.get(i, j);
	}

	public function getK(i:Int, j:Int):Int {
		return Math.floor(i / 2) + j;
	}

	public function getL(i:Int, j:Int):Int {
		return Math.floor((i + 1) / 2) - j;
	}
}
