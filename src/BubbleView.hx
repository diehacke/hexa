package ;

import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

class BubbleView extends UIWidget {
	public static inline var SIZE = 34;
	public static var RATIO = Math.sqrt(3) / 2;

	public var mouseDown:Signal<BubbleView> = new Signal<BubbleView>();
	public var rollOver:Signal<BubbleView> = new Signal<BubbleView>();
	public var rollOut:Signal<BubbleView> = new Signal<BubbleView>();

	private var _movie:Array<UIFrame>;
	private var _top:Shape;

	public function new(bubble:Bubble) {
		super();

		riseFrame = -1;

		_bubble = bubble;

		_movie = _bubble.color().movie();

		_top = new Shape();
		addChild(_top);

		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.ROLL_OVER , onRollOver);
		addEventListener(MouseEvent.ROLL_OUT , onRollOut);
	}

	private var _bubble:Bubble;
	inline public function bubble():Bubble {
		return _bubble;
	}

	override public function redraw():Void {
		if (riseFrame != -1) {
			if (riseFrame < 4) {
				_frame = UIMovies.bubble_blue_rise_0 + riseFrame;
				if (riseFrame < 2) {
					var frame = getCurrentFrame();
					var g = graphics;
					g.clear();
					frame.drawTrimmed(g, 0, 0);
				} else {
					redrawMovie();
				}
			} else {
				_frame = 0;
				redrawMovie();
			}
		} else {
			redrawMovie();
		}
	}

	private var _frame:Int;

	private function getCurrentFrame():UIFrame {
		var result = null;
		if (_movie != null) {
			var numFrames = _movie.length;
			if (numFrames > 0) {
				var index = _frame;
				if (index < 0)
					index = 0;
				else if (index >= numFrames)
					index = numFrames - 1;
				result = _movie[index];
			}
		}
		return result;
	}

	private function redrawMovie():Void {
		var g = graphics;
		g.clear();
		var frame = getCurrentFrame();
		if (frame != null) {
			frame.beginFill(g, 0, 0);
			var w = frame.w();
			var h = frame.h();
			var x0 = frame.offsetX();
			var y0 = frame.offsetY();
			var x1 = frame.offsetX() + w;
			var y1 = frame.offsetY() + h;
			g.moveTo(x0 + w * .25, y0);
			g.lineTo(x1 - w * .25, y0);
			g.lineTo(x1, y0 + w * .25);
			g.lineTo(x1, y1);
			g.lineTo(x0, y1);
			g.lineTo(x0, y0 + w * .25);
			g.endFill();
		}
	}

	private function onMouseDown(event:MouseEvent):Void {
		mouseDown.dispatch(this);
	}

	private function onRollOver(event:MouseEvent):Void {
		rollOver.dispatch(this);
	}

	private function onRollOut(event:MouseEvent):Void {
		rollOut.dispatch(this);
	}

	public var riseFrame:Int;

	private var _explodeFrame:Int;
	private var _exploded:Bool;

	public function explode():Void {
		if (_exploded)
			return;
		_exploded = true;
		_explodeFrame = 0;
		setSelected(false);
		_frame = UIMovies.bubble_blue_down;
		redrawMovie();
		addEventListener(Event.ENTER_FRAME, onEnterFrame);
	}

	private function onEnterFrame(event:Event):Void {
		redrawMovie();
		_frame++;
		_explodeFrame++;
		if (_explodeFrame > 5) {
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			if (parent != null)
				parent.removeChild(this);
		}
	}

	private var _selected:Bool;
	inline public function selected():Bool {
		return _selected;
	}
	public function setSelected(value:Bool):Void {
		if (_selected != value) {
			_selected = value;
			_highlightRatio = _selected ? 1 : 0;
			updateHighlight();
		}
	}

	private var _highlightRatio:Float = 0;

	private function updateHighlight():Void {
		var g = _top.graphics;
		g.clear();
		if (_highlightRatio > .1) {
			g.lineStyle(2, _bubble.color().value(), .5);
			g.beginFill(0xffffff, .8);
			g.drawCircle(0, 0, SIZE * .4 - 4);
			g.endFill();
		}
	}

	private var _motions:Array<Motion> = new Array<Motion>();

	public function updateMotions():Void {
		var x = _x;
		var y = _y;
		for (motion in _motions) {
			x += motion.x;
			y += motion.y;
		}
		this.x = x;
		this.y = y;
	}

	private var _targetX:Float = 0;
	private var _targetY:Float = 0;
	private var _x:Float = 0;
	private var _y:Float = 0;

	public function immediateMotion(x:Float, y:Float):Void {
		for (motion in _motions) {
			motion.tween.kill();
		}
		_motions = new Array<Motion>();
		_targetX = x;
		_targetY = y;
		_x = x;
		_y = y;
	}

	public function pushMotion(targetX:Float, targetY:Float, hasDelay:Bool):Void {
		if (_motions.length > 1) {
			for (motion in _motions) {
				motion.tween.framesSpeed++;
			}
		}
		var sumTargetX = _targetX;
		var sumTargetY = _targetY;
		var sumX = _x;
		var sumY = _y;
		for (motion in _motions) {
			sumTargetX += motion.targetX;
			sumTargetY += motion.targetY;
			sumX += motion.x;
			sumY += motion.y;
		}
		var motion = new Motion();
		motion.targetX = targetX - sumTargetX;
		motion.targetY = targetY - sumTargetY;
		motion.x = x - sumX;
		motion.y = y - sumY;
		motion.tween = Tween.to(motion, 20, {x:motion.targetX, y:motion.targetY}, null, true)
			.setWait(hasDelay ? 15 : 0)
			.setEase(Tween.easeOutBounce)
			.setOnComplete(onMotionComplete);
		_motions.push(motion);
	}

	private function onMotionComplete(tween:Tween<Motion>):Void {
		var motion = tween.target();
		_motions.remove(motion);
		_targetX += motion.targetX;
		_targetY += motion.targetY;
		_x += motion.x;
		_y += motion.y;
	}
}
