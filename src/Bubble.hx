package ;

class Bubble {
	public function new(color:Color) {
		_color = color;
	}

	private var _color:Color;
	inline public function color():Color {
		return _color;
	}

	private var _cell:Cell;
	inline public function cell():Cell {
		return _cell;
	}

	public var view:BubbleView;

	public function toString():String {
		return "<Bubble color=" + _color + ">";
	}
}
