package ;

import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

class ConfirmButton extends UIWidget {
	public var confirmed:Signal<ConfirmButton> = new Signal<ConfirmButton>();

	private var _container:Sprite;
	private var _button:GameButton;

	public function new(image:UIFrame) {
		super();

		_enabled = true;
		_allowSelected = true;

		_container = new Sprite();
		addChild(_container);

		_button = new GameButton();
		_button.image = image;
		_button.redraw();
		_button.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
		_button.addEventListener(MouseEvent.CLICK, onButtonClick);
		_container.addChild(_button);

		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
	}

	private function onAddedToStage(event:Event):Void {
		stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDown);
	}

	private function onRemovedFromStage(event:Event):Void {
		stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDown);
	}

	private function onStageMouseDown(event:MouseEvent):Void {
		if (mouseX > 0 && mouseX < w && mouseY > 0 && mouseY < h)
			return;
		if (_panel != null) {
			var button = _panel.yes();
			if (button.mouseX > 0 && button.mouseX < w && button.mouseY > 0 && button.mouseY < h)
				return;
			var button = _panel.no();
			if (button.mouseX > 0 && button.mouseX < w && button.mouseY > 0 && button.mouseY < h)
				return;
		}
		setPanel(false);
	}

	override public function redraw():Void {
		w = _button.w;
		h = _button.h;
	}

	private var _allowSelected:Bool;

	public function setAllowSelected(value:Bool):Void {
		if (_allowSelected != value) {
			_allowSelected = value;
			updateSelected();
		}
	}

	private function onButtonMouseDown(event:MouseEvent):Void {
		dispatchEvent(new SoundEvent(SoundEvent.PRESS));
		if (needConfirm)
			setPanel(!_isPanel);
	}

	private function onButtonClick(event:MouseEvent):Void {
		if (!needConfirm)
			confirmed.dispatch(this);
	}

	public var needConfirm:Bool;

	private var _isPanel:Bool;
	private var _panel:ConfirmPanel;
	private var _mask:Shape;

	private function setPanel(value:Bool):Void {
		if (_isPanel != value) {
			_isPanel = value;
			var panelStartX = -150;
			if (_isPanel) {
				if (_panel == null) {
					_panel = new ConfirmPanel();
					_panel.yes().addEventListener(MouseEvent.MOUSE_DOWN, onYesClick);
					_panel.no().addEventListener(MouseEvent.MOUSE_DOWN, onNoClick);
					_panel.redraw();
					_container.addChildAt(_panel, 0);

					_mask = new Shape();
					var g = _mask.graphics;
					g.beginFill(0xffff00);
					g.drawRect(0, -5, 270, h + 10);
					g.endFill();
					_panel.mask = _mask;
					_panel.x = panelStartX;
					_container.addChild(_mask);
				}
				Tween.to(_panel, 150, {x:52})
					.setEase(Tween.easeOut);
			} else {
				if (_panel != null) {
					Tween.to(_panel, 200, {x:panelStartX})
						.setEase(Tween.easeOut)
						.setVoidOnComplete(onHideTweenComplete);
				}
			}
			dispatchEvent(new SoundEvent(SoundEvent.PRESS));
		}
	}

	private function onHideTweenComplete():Void {
		_container.removeChild(_panel);
		_panel = null;
		_container.removeChild(_mask);
		_mask = null;
	}

	private function onYesClick(event:MouseEvent):Void {
		setPanel(false);
		confirmed.dispatch(this);
	}

	private function onNoClick(event:MouseEvent):Void {
		setPanel(false);
	}

	private var _enabled:Bool;
	inline public function enabled():Bool {
		return _enabled;
	}
	public function setEnabled(value:Bool):Void {
		if (_enabled != value) {
			_enabled = value;
			_button.setEnabled(_enabled);
			updateEnabledAndSelected(false);
		}
	}

	public function setEnabledImmediate(value:Bool):Void {
		if (_enabled != value) {
			_enabled = value;
			updateEnabledAndSelected(true);
		}
	}

	private var _selected:Bool;
	inline public function selected():Bool {
		return _selected;
	}
	public function setSelected(value:Bool):Void {
		if (_selected != value) {
			_selected = value;
			updateEnabledAndSelected(false);
		}
	}

	private function updateEnabledAndSelected(immediate:Bool):Void {
		if (_enabled) {
			var vars = {x:0};
			if (immediate) {
				Tween.apply(_container, vars);
			} else {
				Tween.to(_container, 200, vars)
					.setEase(Tween.easeOutBounce);
			}
		} else {
			var vars = {x:-150};
			if (immediate) {
				Tween.apply(_container, vars);
			} else {
				Tween.to(_container, 200, vars)
					.setEase(Tween.easeOut);
			}
			setPanel(false);
		}
		updateSelected();
	}

	private var _oldSelected:Bool;

	private function updateSelected():Void {
		var selected = _enabled && _selected && _allowSelected;
		if (_oldSelected != selected) {
			_oldSelected = selected;
			if (_oldSelected) {
				Tween.to(_button, 150, {y:-20})
					.setEase(Tween.easeOut)
					.setVoidOnComplete(onSelectedTweenComplete0);
			} else {
				Tween.to(_button, 150, {y:0})
					.setEase(Tween.easeOut);
			}
		}
	}

	private function onSelectedTweenComplete0():Void {
		Tween.to(_button, 200, {y:0})
			.setEase(Tween.easeIn)
			.setVoidOnComplete(onSelectedTweenComplete1);
	}

	private function onSelectedTweenComplete1():Void {
		Tween.to(_button, 100, {y:-20})
			.setEase(Tween.easeOut)
			.setVoidOnComplete(onSelectedTweenComplete0);
	}
}
