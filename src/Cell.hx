package ;

class Cell {
	public function new(i:Int, j:Int) {
		_i = i;
		_j = j;
	}

	private var _i:Int;
	inline public function i():Int {
		return _i;
	}

	private var _j:Int;
	inline public function j():Int {
		return _j;
	}

	private var _bubble:Bubble;
	inline public function bubble():Bubble {
		return _bubble;
	}
	public function setBubble(value:Bubble):Void {
		var bubble = _bubble;
		if (bubble != null) {
			if (bubble.cell() != null)
				bubble.cell()._bubble = null;
			BubblePM.setCell(bubble, null);
		}
		if (value != null) {
			if (value.cell() != null)
				value.cell()._bubble = null;
			BubblePM.setCell(value, this);
		}
		_bubble = value;
	}

	public function getX():Float {
		getXY(_i, _j);
		return _outX;
	}

	public function getY():Float {
		getXY(_i, _j);
		return _outY;
	}

	private static var _matrix:RotationMatrix = new RotationMatrix().rotate(.2);
	private static var _outX:Float;
	private static var _outY:Float;

	private static function getXY(i:Int, j:Int):Void {
		var x = i * BubbleView.SIZE;
		var y = (j * BubbleView.SIZE + (i % 2 == 0 ? BubbleView.SIZE * .5 : .0)) / BubbleView.RATIO;
		_matrix.transform(x, y);
		_outX = _matrix.outX + 100;
		_outY = _matrix.outY - 70;
	}

	public function toString():String {
		return "<Cell i=" + _i + " j=" + _j + ">";
	}

	public function init(grid:Grid<Cell>):Void {
		_neighbours = new Array<Cell>();
		if (_i % 2 == 0) {
			addNeighbour(grid, _i - 1, _j);
			addNeighbour(grid, _i - 1, _j + 1);

			addNeighbour(grid, _i, _j - 1);
			addNeighbour(grid, _i, _j + 1);

			addNeighbour(grid, _i + 1, _j);
			addNeighbour(grid, _i + 1, _j + 1);

			_rightBottom = grid.get(_i + 1, _j + 1);
		} else {
			addNeighbour(grid, _i - 1, _j - 1);
			addNeighbour(grid, _i - 1, _j);

			addNeighbour(grid, _i, _j - 1);
			addNeighbour(grid, _i, _j + 1);

			addNeighbour(grid, _i + 1, _j - 1);
			addNeighbour(grid, _i + 1, _j);

			_rightBottom = grid.get(_i + 1, _j);
		}
	}

	private function addNeighbour(grid:Grid<Cell>, i:Int, j:Int):Void {
		var cell = grid.get(i, j);
		if (cell != null)
			_neighbours.push(cell);
	}

	private var _neighbours:Array<Cell>;
	inline public function neighbours():Iterable<Cell> {
		return _neighbours;
	}

	private var _rightBottom:Cell;
	inline public function rightBottom():Cell {
		return _rightBottom;
	}
}

private class BubblePM extends Bubble {
	inline public static function setCell(bubble:Bubble, cell:Cell):Void {
		bubble._cell = cell;
	}
}
