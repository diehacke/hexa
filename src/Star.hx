package ;

import flash.display.Shape;

class Star extends UIWidget {
	private var _index:Int;

	public function new(index:Int) {
		super();

		_index = index;
	}

	override public function redraw():Void {
		var frame = switch (_index) {
			case 0: UIFrames.stars_slot_1;
			case 1: UIFrames.stars_slot_2;
			default: UIFrames.stars_slot_3;
		};
		var g = graphics;
		g.clear();
		frame.draw(g, 0, 0);
	}

	private function createStar():Void {
		_star = new Shape();
		var frame = switch (_index) {
			case 0: UIFrames.stars_star_1;
			case 1: UIFrames.stars_star_2;
			default: UIFrames.stars_star_3;
		};
		frame.draw(_star.graphics, 0, 0);
		addChild(_star);
	}

	private var _star:Shape;

	public function show(delay:Int):Void {
		if (_star != null)
			return;

		createStar();

		var scale = 2;
		_star.y = -50;
		_star.scaleX = scale;
		_star.scaleY = scale;
		_star.alpha = 0;
		Tween.to(_star, 300, {y:0, scaleX:1, scaleY:1, alpha:1})
			.setWait(delay)
			.setEase(Tween.easeIn)
			.setVoidOnComplete(playSound);
	}

	public function showImmediate():Void {
		if (_star != null)
			return;
		createStar();
	}

	private function playSound():Void {
		switch (_index) {
			case 0: dispatchEvent(new SoundEvent(SoundEvent.STAR_0));
			case 1: dispatchEvent(new SoundEvent(SoundEvent.STAR_1));
			default: dispatchEvent(new SoundEvent(SoundEvent.STAR_2));
		}
	}
}
