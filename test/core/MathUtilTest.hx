package core;

class MathUtilTest {
	private static inline var PRECISION:Float = .00001;

	@Test
	public function getAngle():Void {
		Assert.areClose(MathUtil.getAngle(1, 0, 1, 0), 0, PRECISION);
		Assert.areClose(MathUtil.getAngle(1, 0, 0, 1), Math.PI / 2, PRECISION);
		Assert.areClose(MathUtil.getAngle(1, 0, 0, -1), -Math.PI / 2, PRECISION);

		Assert.areClose(MathUtil.getAngle(1, 0, 2, 0), 0, PRECISION);
		Assert.areClose(MathUtil.getAngle(3, 0, 0, 1), Math.PI / 2, PRECISION);
		Assert.areClose(MathUtil.getAngle(100, 0, 0, -1), -Math.PI / 2, PRECISION);

		Assert.areClose(MathUtil.getAngle(100, 1, -1, 100), Math.PI / 2, PRECISION);
	}

	@Test
	public function segmentsCrossing():Void {
		Assert.areEqual(MathUtil.segmentsCrossing(0, 0, 2, 0, 1, -1, 1, 1), true);
		Assert.areEqual(MathUtil.outX, 1);
		Assert.areEqual(MathUtil.outY, 0);

		Assert.areEqual(MathUtil.segmentsCrossing(0, 0, 2, 0, 3, -1, 3, 1), false);
		Assert.areEqual(MathUtil.outX, 0);
		Assert.areEqual(MathUtil.outY, 0);
		
		Assert.areEqual(MathUtil.segmentsCrossing(-2, -3, 2, 3, -2, 2, 4, 1), true);
		Assert.areEqual(MathUtil.outX, 1);
		Assert.areEqual(MathUtil.outY, 1.5);

		Assert.areEqual(MathUtil.segmentsCrossing(1, 10, 4, 4, 3, 7, 6, 6), false);
		Assert.areEqual(MathUtil.outX, 0);
		Assert.areEqual(MathUtil.outY, 0);
	}
}
