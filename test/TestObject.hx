package;

class TestObject {
	private var _name:String;

	public function new(name:String) {
		_name = name;
	}

	public function toString():String {
		return "(" + _name + ")";
	}
}
