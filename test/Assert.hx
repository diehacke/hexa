package ;

import haxe.PosInfos;

class Assert {
	public static function isTrue(value:Bool, ?info:PosInfos):Void {
		massive.munit.Assert.isTrue(value, info);
	}
	
	public static function isFalse(value:Bool, ?info:PosInfos):Void {
		massive.munit.Assert.isFalse(value, info);
	}
	
	public static function isNull(value:Dynamic, ?info:PosInfos):Void {
		massive.munit.Assert.isNull(value, info);
	}
	
	public static function isNotNull(value:Dynamic, ?info:PosInfos):Void {
		massive.munit.Assert.isNotNull(value, info);
	}
	
	public static function isNaN(value:Float, ?info:PosInfos):Void {
		massive.munit.Assert.isNaN(value, info);
	}

	public static function isNotNaN(value:Float, ?info:PosInfos):Void {
		massive.munit.Assert.isNotNaN(value, info);
	}
	
	public static function isType(value:Dynamic, type:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.isType(value, type, info);
	}
	
	public static function isNotType(value:Dynamic, type:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.isNotType(value, type, info);
	}
	
	public static function areEqual(expected:Dynamic, actual:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.areEqual(expected, actual, info);
	}
	
	public static function areNotEqual(expected:Dynamic, actual:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.areNotEqual(expected, actual, info);
	}

	public static function areSame(expected:Dynamic, actual:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.areSame(expected, actual, info);
	}

	public static function areNotSame(expected:Dynamic, actual:Dynamic, ?info:PosInfos):Void
	{
		massive.munit.Assert.areNotSame(expected, actual, info);
	}

	public static function fail(msg:String, ?info:PosInfos):Void
	{
		massive.munit.Assert.fail(msg, info);
	}

	public static function areClose(expected:Float, actual:Float, precision:Float, ?info:PosInfos):Void
	{
		if (Math.abs(actual - expected) < precision)
			isTrue(true);
		else
			fail("Value [" + actual +"] was not close to expected value [" + expected + "]");
	}
}
