package game;

class CellTest {
	@Test
	public function test01() {
		var cell0 = new Cell(0, 0);
		var bubble0 = new Bubble(Color.RED);
		var bubble1 = new Bubble(Color.GREEN);

		cell0.setBubble(bubble0);
		Assert.areEqual(bubble0, cell0.bubble());
		Assert.areEqual(bubble0.cell(), cell0);

		cell0.setBubble(bubble1);
		Assert.areEqual(bubble1, cell0.bubble());
		Assert.areEqual(bubble1.cell(), cell0);
		Assert.areEqual(bubble0.cell(), null);
	}

	@Test
	public function test02() {
		var cell0 = new Cell(0, 0);
		var cell1 = new Cell(1, 0);
		var bubble0 = new Bubble(Color.RED);

		cell0.setBubble(bubble0);
		Assert.areEqual(bubble0.cell(), cell0);
		Assert.areEqual(null, cell1.bubble());
		cell1.setBubble(bubble0);
		Assert.areEqual(bubble0, cell1.bubble());
		Assert.areEqual(bubble0.cell(), cell1);
		Assert.areEqual(null, cell0.bubble());
	}
}
