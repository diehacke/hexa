package game;

class GridTest {
	@Test
	public function test01() {
		var grid = new Grid<String>(2, 2);
		grid.set(0, 0, "a");
		grid.set(1, 0, "b");
		grid.set(0, 1, "c");
		grid.set(1, 1, "d");
		Assert.areEqual(2, grid.sizeI());
		Assert.areEqual(2, grid.sizeJ());
		Assert.areEqual("abcd", [grid.get(0, 0), grid.get(1, 0), grid.get(0, 1), grid.get(1, 1)].join(""));
	}

	@Test
	public function test02() {
		var grid = new Grid<String>(2, 2);
		grid.set(0, 0, "a");
		grid.set(1, 0, "b");
		grid.set(0, 1, "c");
		grid.set(1, 1, "d");

		grid.set(-1, 0, "#");
		grid.set(2, 0, "#");
		grid.set(0, -1, "#");
		grid.set(1, -1, "#");
		grid.set(1, 2, "#");
		grid.set(-1, 2, "#");
		Assert.areEqual("abcd", [grid.get(0, 0), grid.get(1, 0), grid.get(0, 1), grid.get(1, 1)].join(""));
		Assert.isNull(grid.get(-1, 0));
		Assert.isNull(grid.get(2, 0));
		Assert.isNull(grid.get(0, -1));
		Assert.isNull(grid.get(1, -1));
		Assert.isNull(grid.get(1, 2));
		Assert.isNull(grid.get(-1, 2));
		Assert.isNull(grid.get(-10, 20));
	}

	@Test
	public function test03() {
		var grid = new Grid<String>(3, 2);
		grid.set(0, 0, "a");
		grid.set(1, 0, "b");
		grid.set(2, 0, "c");
		grid.set(0, 1, "d");
		grid.set(1, 1, "e");
		grid.set(2, 1, "f");
		Assert.areEqual(3, grid.sizeI());
		Assert.areEqual(2, grid.sizeJ());
		Assert.areEqual("a", grid.get(0, 0));
		Assert.areEqual("b", grid.get(1, 0));
		Assert.areEqual("c", grid.get(2, 0));
		Assert.areEqual("d", grid.get(0, 1));
		Assert.areEqual("e", grid.get(1, 1));
		Assert.areEqual("f", grid.get(2, 1));
	}

	@Test
	public function test04() {
		var grid = new Grid<String>(2, 3);
		grid.set(0, 0, "a");
		grid.set(1, 0, "b");
		grid.set(0, 1, "c");
		grid.set(1, 1, "d");
		grid.set(0, 2, "e");
		grid.set(1, 2, "f");
		Assert.areEqual(2, grid.sizeI());
		Assert.areEqual(3, grid.sizeJ());
		Assert.areEqual("a", grid.get(0, 0));
		Assert.areEqual("b", grid.get(1, 0));
		Assert.areEqual("c", grid.get(0, 1));
		Assert.areEqual("d", grid.get(1, 1));
		Assert.areEqual("e", grid.get(0, 2));
		Assert.areEqual("f", grid.get(1, 2));
	}
}
