package;

import massive.munit.TestRunner;
import massive.munit.TestSuite;

class TestMain {
	static function main() {
		new TestMain();
	}

	public function new() {
		var suites = new Array<Class<TestSuite>>();
		suites.push(HTestSuite);

		var client = new CustomResultClient();
		var runner:TestRunner = new TestRunner(client); 
#if test_debug
		runner.debug(suites);
#else
		runner.run(suites);
#end
	}
}
