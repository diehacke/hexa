package;

import haxe.PosInfos;
import massive.munit.ITestResultClient;
import massive.munit.TestResult;
import massive.munit.util.MathUtil;

class CustomResultClient implements ITestResultClient {
	public static inline var DEFAULT_ID:String = "Custom output";

	public var id(default, null):String;

	private var _completionHandler:ITestResultClient -> Void;
	public var completionHandler(get, set):ITestResultClient -> Void;
	private function get_completionHandler():ITestResultClient -> Void {
		return _completionHandler;
	}
	private function set_completionHandler(value:ITestResultClient -> Void):ITestResultClient -> Void {
		return _completionHandler = value;
	}

	private var failures:String;
	private var errors:String;
	private var ignored:String;
	private var output:String;
	private var currentTestClass:String;
	private var originalTrace:Dynamic;

	public function new() {
		id = DEFAULT_ID;
		init();
		print("MUnit Results");
		print("------------------------------");
	}

	function processComplete() {
		flash.Lib.fscommand("quit");
	}

	private function init():Void {
		originalTrace = haxe.Log.trace;
		haxe.Log.trace = customTrace;
		output = "";
		failures = "";
		errors = "";
		ignored = "";
		currentTestClass = "";
	}

	public function addPass(result:TestResult):Void {
		checkForNewTestClass(result);
		printOnLine(result.passed ? "v" : "x");
	}

	public function addFail(result:TestResult):Void {
		checkForNewTestClass(result);
		failures += result.failure;

		var info:PosInfos = result.failure.info;
		var lineNumber = info != null ? info.lineNumber : 0;
		print("test/" + result.className.split(".").join("/") + ".hx" + ":" + lineNumber +
		":" + result.failure);
	}

	public function addIgnore(result:TestResult):Void {
		checkForNewTestClass(result);
		print(",");
		ignored += "test/" + result.className.split(".").join("/") + ".hx" +
		":0:Ignored: " + result.location + " - " + result.description;
	}

	public function addError(result:TestResult):Void {
		checkForNewTestClass(result);
		errors += "3:" + result.error;
	}

	public function reportFinalStatistics(
		testCount:Int, passCount:Int, failCount:Int, errorCount:Int, ignoreCount:Int, time:Float):Dynamic {
		printExceptions();

		if (ignoreCount > 0) {
			print("------------------------------");
			print("Ignored tests:" + ignored);
			print("------------------------------");
		} else {
			print("");
		}

		print((passCount == testCount) ? "PASSED" : "3:FAILED");

		var text = "Tests: " + testCount + " Passed: " + passCount + " Failed: " +
		failCount + " Errors: " + errorCount + " Time: " + MathUtil.round(time, 5);
		if (ignoreCount > 0) {
			text += " | Ignored: " + ignoreCount;
		}

		print(text);
		print("==============================");

		haxe.Log.trace = originalTrace;
		if (completionHandler != null) completionHandler(this);
		processComplete();
		return output;
	}

	private function checkForNewTestClass(result:TestResult):Void {
		if (result.className != currentTestClass) {
			printExceptions();
			currentTestClass = result.className;
			flushOnLine();
			printOnLine(currentTestClass + ": ");
		}
	}

	private function printExceptions():Void {
		if (errors != "") {
			print(errors);
			errors = "";
		}
		if (failures != "") {
			print(failures);
			failures = "";
		}
	}

	private var printOnLineText:String;

	private function printOnLine(value:Dynamic):Void {
		if (printOnLineText == null)
			printOnLineText = "";
		printOnLineText += value;
	}

	private function flushOnLine() {
		if (printOnLineText != null) {
			flash.Lib.trace(printOnLineText);
			output += printOnLineText;
			printOnLineText = null;
		}
	}

	private function print(value:Dynamic):Void {
		flushOnLine();
		flash.Lib.trace(value);
		output += value;
	}

	private function customTrace(value, ?info:haxe.PosInfos) {
		print("\ntest/" + info.className.split(".").join("/") + ".hx:" + info.lineNumber + ": " + value);
	}
}
