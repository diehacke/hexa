var hexa = "file:///Z|/home/user/hexa/";
var doc = function() {
	fl.openDocument(hexa + "assets/graphics.fla");
	var uiDoc = fl.getDocumentDOM();

	fl.createDocument();
	var newDoc = fl.getDocumentDOM();

	for (var i in uiDoc.library.items) {
		var item = uiDoc.library.items[i];
		uiDoc.library.addItemToDocument({x:0, y:0}, item.name);
		uiDoc.clipCut();
		newDoc.clipPaste();
		newDoc.clipCut();
	}

	newDoc.width = 1024;
	newDoc.height = 1024;
	newDoc.backgroundColor = "#000000";
	return newDoc;
}();
var tolerance = 2;
var rects = [];
var movies = [];
var timeline = doc.timelines[doc.currentTimeline];
timeline.currentLayer = 0;
timeline.currentFrame = 0;
var layer = timeline.layers[timeline.currentLayer];
var frame = layer.frames[timeline.currentFrame];
for (var i in doc.library.items) {
	var item = doc.library.items[i];
	var childTimeline = item.timeline;
	if (childTimeline) {
		var movie = null;
		for (var j = 0; j < childTimeline.frameCount; j++) {
			var frameJ = childTimeline.layers[0].frames[j];
			if (frameJ.name) {
				doc.library.addItemToDocument({x:0, y:0}, item.name);
				var element = frame.elements[rects.length];
				var m = element.matrix;
				m.tx = 0;
				m.ty = 0;
				element.matrix = m;
				var rect = {};
				rect.name = item.name + "_" + frameJ.name;
				rect.frameName = frameJ.name;
				rect.firstFrame = j;
				rects.push(rect);
				element.symbolType = "graphic";
				if (movie == null) {
					movie = {};
					movie.name = item.name;
					movie.rects = [];
				}
				movie.rects.push(rect);
			}
		}
		if (movie != null) {
			movies.push(movie);
		}
	}
}
for (var i in rects) {
	var rect = rects[i];
	var element = frame.elements[i];
	element.firstFrame = rect.firstFrame;
	doc.selectNone();
	doc.selection = [element];
	var selection = doc.getSelectionRect();
	rect.index = i;
	rect.w = Math.round(selection.right - selection.left + tolerance * 2);
	rect.h = Math.round(selection.bottom - selection.top + tolerance * 2);
	rect.offsetX = Math.round(selection.left) - tolerance;
	rect.offsetY= Math.round(selection.top) - tolerance;
}
rects.sort(function(a, b) {
	return a.h - b.h;
});
var rectsToArrange = rects.concat();
var pages = [];
var atlasIndex = 0;
while (true) {
	var isOver = true;
	var arrangedRects = [];
	var offsetX = 0;
	var offsetY = 0;
	var lineH = 0;
	var lineW = 0;
	while (true) {
		var rect = rectsToArrange.pop();
		if (!rect)
			break;
		if (offsetX + rect.w > doc.width - 2) {
			offsetX = 0;
			offsetY += lineH + 2;
			lineH = 0;
		}
		rect.x = Math.round(offsetX - rect.offsetX);
		rect.y = Math.round(offsetY - rect.offsetY);
		offsetX += rect.w + 2;
		if (lineH < rect.h)
			lineH = rect.h;
		if (offsetY + lineH > doc.height) {
			isOver = false;
			rectsToArrange.push(rect);
			break;
		}
		rect.atlasIndex = atlasIndex;
		arrangedRects.push(rect);
	}
	for (var i = 0; i < arrangedRects.length; i++) {
		var rect = arrangedRects[i];
		var element = frame.elements[rect.index];
		var m = element.matrix;
		m.tx = rect.x;
		m.ty = rect.y;
		element.matrix = m;
	}
	pages.push(arrangedRects);
	atlasIndex++;
	if (isOver)
		break;
}

for (var i = 0; true; i++) {
	var file = hexa + "assets/images/atlas_" + i + ".png";
	if (!FLfile.exists(file))
		break;
	FLfile.remove(file);
}

for (var i = 0; i < rects.length; i++) {
	var rect = rects[i];
	var element = frame.elements[rect.index];
	element.colorAlphaPercent = 0;
}
for (var j = 0; j < pages.length; j++) {
	var arrangedRects = pages[j];
	for (var i = 0; i < arrangedRects.length; i++) {
		var rect = arrangedRects[i];
		var element = frame.elements[rect.index];
		element.colorAlphaPercent = 100;
		element.symbolType = "movie clip";
	}
	for (var i = 0; i < arrangedRects.length; i++) {
		var rect = arrangedRects[i];
		var element = frame.elements[rect.index];
		element.symbolType = "graphic";
	}
	for (var i = 0; i < arrangedRects.length; i++) {
		var rect = arrangedRects[i];
		var element = frame.elements[rect.index];
		element.firstFrame = rect.firstFrame;
	}
	doc.exportPNG(hexa + "assets/images/atlas_" + j + ".png", true, true);
	for (var i = 0; i < arrangedRects.length; i++) {
		var rect = arrangedRects[i];
		var element = frame.elements[rect.index];
		element.colorAlphaPercent = 0;
	}
}

var text = "";
text += "// <<Generated>>\n"
text += "package ;\n"
text += "\n";
text += "class UIFrames {\n";
text += "\tpublic static function init():Void {\n";
var bdIndex = -1;
for (var i in rects) {
	var rect = rects[i];
	if (bdIndex != rect.atlasIndex) {
		bdIndex = rect.atlasIndex;
		text += "\t\tvar bd = UIImages.atlas_" + bdIndex + ";\n";
	}
	text += "\t\t" + rect.name + ".init(bd);\n";
}
text += "\t}\n";
text += "\n";
for (var i in rects) {
	var rect = rects[i];
	text += "\tpublic static var " + rect.name + ":UIFrame = new UIFrame(";
	text += rect.x + ", ";
	text += rect.y + ", ";
	text += Math.round(rect.w - tolerance * 2) + ", ";
	text += Math.round(rect.h - tolerance * 2) + ", ";
	text += Math.round(rect.offsetX + tolerance) + ", ";
	text += Math.round(rect.offsetY + tolerance) + ");\n";
}
text += "}";
FLfile.write(hexa + "src-core/UIFrames.hx", text);

var getClassName = function(text) {
	var items = text.split("_");
	for (var i = 0; i < items.length; i++) {
		var part = items[i];
		items[i] = part.charAt(0).toUpperCase() + part.substr(1);
	}
	return items.join("");
}
var text = "";
text += "// <<Generated>>\n"
text += "package ;\n"
text += "\n";
text += "class UIMovies {\n";
for (var i in movies) {
	if (i != 0)
		text += "\n";
	var movie = movies[i];
	var className = getClassName(movie.name);
	for (var j in movie.rects) {
		var rect = movie.rects[j];
		text += "\tinline public static var " + rect.name + ":Int = " + j + ";\n";
	}
	text += "\n";
	text += "\tpublic static var " + movie.name + ":Array<UIFrame> = [\n";
	for (var j in movie.rects) {
		var rect = movie.rects[j];
		text += "\t\tUIFrames." + rect.name + (j != movie.rects.length - 1 ? "," : "") + "\n";
	}
	text += "\t];\n";
}
text += "}\n";
FLfile.write(hexa + "src-core/UIMovies.hx", text);
