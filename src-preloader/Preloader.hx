package ;

import flash.Lib;
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;
import flash.display.StageScaleMode;
import flash.display.StageAlign;
import flash.display.LineScaleMode;
import flash.display.CapsStyle;
import flash.display.Graphics;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.filters.ColorMatrixFilter;

class Preloader extends Sprite {
	private var _bubble:Bitmap;

    public function new() {
        super();

		_bubble = new Bitmap(new BubbleRed(0, 0));
		addChild(_bubble);

		addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
    }

	private function onAddedToStage(event:Event):Void {
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;

        addEventListener(Event.ENTER_FRAME, onLoadProgress);
		stage.addEventListener(Event.RESIZE, onStageResize);
		resize();
	}

	private function onRemovedFromStage(event:Event):Void {
        removeEventListener(Event.ENTER_FRAME, onLoadProgress);
		stage.removeEventListener(Event.RESIZE, onStageResize);
	}

	private function onStageResize(event:Event = null):Void {
		resize();
	}

	private function resize():Void {
		_bubble.x = stage.stageWidth / 2 - _bubble.width / 2;
		_bubble.y = stage.stageHeight / 2 - _bubble.height / 2;

		var g = graphics;
		g.clear();
		g.beginFill(0xffffff);
		g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		g.endFill();
	}

    private function onLoadProgress(event:Event):Void {
        var bytesLoaded = Lib.current.stage.loaderInfo.bytesLoaded;
        var bytesTotal = Lib.current.stage.loaderInfo.bytesTotal;
        var loaded = bytesLoaded >= bytesTotal;

		var g = graphics;
		g.clear();
		resize();
        if (loaded) {
			graphics.clear();
            removeEventListener(Event.ENTER_FRAME, onLoadProgress);
            Lib.current.removeChild(this);
            var cls = Type.resolveClass("Main");
            var app = Type.createInstance(cls, []);
            Lib.current.addChild(app);
        }

		var x0 = Lib.current.stage.stageWidth * .5;
		var y0 = Lib.current.stage.stageHeight * .5;

		var ratio = bytesLoaded / bytesTotal;

		g.lineStyle(4, 0xcccccc);
		g.drawCircle(x0, y0, 100);
		g.lineStyle();

		_bubble.filters = [new ColorMatrixFilter([
			.3 + ratio * .7, .3 * (1 - ratio), .3 * (1 - ratio), 0, 0,
			.3 * (1 - ratio), .4 + ratio * .6, .3 * (1 - ratio), 0, 0,
			.3 * (1 - ratio), .3 * (1 - ratio), .4 + ratio * .6, 0, 0,
			0, 0, 0, 1, 0
		])];

		g.lineStyle(8, 0x202080, 1, false, LineScaleMode.NORMAL, CapsStyle.SQUARE);
		var angle0 = -Math.PI * .5;
		var angle1 = angle0 + ratio * Math.PI * 2;
		g.moveTo(x0 + Math.cos(angle0) * 100, y0 + Math.sin(angle0) * 100);
		drawArc(g, x0, y0, 100, angle0, angle1);
    }

    public static function main() {
        Lib.current.addChild(new Preloader());
    }

	public static function drawArc(g:Graphics, x:Float, y:Float, radius:Float, angle0:Float, angle1:Float):Void {
		var numSegments = Math.ceil((angle1 - angle0) * 4 / Math.PI);
		if (numSegments < 0) {
			numSegments = -numSegments;
		}
		var deltaAngle = (angle1 - angle0) / numSegments;
		var externalRadius = radius / Math.cos(deltaAngle * .5);
		for (i in 0 ... numSegments) {
			var angle = angle0 + (i + 1) * deltaAngle;
			var halfAngle = angle0 + (i + .5) * deltaAngle;
			var x1 = x + Math.cos(halfAngle) * externalRadius;
			var y1 = y + Math.sin(halfAngle) * externalRadius;
			var x2 = x + Math.cos(angle) * radius;
			var y2 = y + Math.sin(angle) * radius;
			g.curveTo(x1, y1, x2, y2);
		}
	}
}

@:bitmap("bubble_red.png")
class BubbleRed extends flash.display.BitmapData
{
}
