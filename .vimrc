map mt :silent !ctags -R --language-force=haxe --exclude=_std -R src/ src-core/ /usr/lib/haxe/std/flash/ /usr/lib/haxe/std/haxe/ /usr/lib/haxe/std/sys/ /usr/lib/haxe/std/tools/ /usr/lib/haxe/std/*.hx<cr>

map git :silent !gnome-terminal &<cr>
map ,log :silent !gnome-terminal -x sh -c "tail -f ~/.macromedia/Flash_Player/Logs/flashlog.txt" &<cr>

" munit
function SaveAndRunTests(test_debug)
	if expand("%") != ''
		:w
	endif

	if a:test_debug
		cgete system('haxe make-test.hxml -D test_debug')
	else
		cgete system('haxe make-test.hxml')
	endif
	copen
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('~/flashplayer/flashplayerdebugger test-bin/test.swf')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('cat ~/.macromedia/Flash_Player/Logs/flashlog.txt')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	:Highlight 5 PASSED$
	:Highlight 7 FAILED$
endfunction
map ,t :call SaveAndRunTests(0)<cr>
map ,dt :call SaveAndRunTests(1)<cr>

function SaveAndRunGame()
	if expand("%") != ''
		:w
	endif

	cgete system('haxe make-debug.hxml')
	copen
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('~/flashplayer/flashplayerdebugger bin/hexa_debug.swf')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('cat ~/.macromedia/Flash_Player/Logs/flashlog.txt')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
endfunction

function SaveAndRunRelease()
	if expand("%") != ''
		:w
	endif

	cgete system('haxe make-release.hxml')
	copen
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('~/flashplayer/flashplayerdebugger bin-release/hexa.swf')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
	redraw

	caddexpr system('cat ~/.macromedia/Flash_Player/Logs/flashlog.txt')
	if v:shell_error
		return v:shell_error
	endif
	100500 " scroll to bottom
endfunction
map ,g :call SaveAndRunGame()<cr>
map ,j :call SaveAndRunRelease()<cr>
